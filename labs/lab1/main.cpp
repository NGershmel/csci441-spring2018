#include <iostream>
#include <stdio.h>

#include "bitmap_image.hpp"

using namespace std;

class Point
{
public:
	float x, y, r, g, b;
	Point(float x_in, float y_in, float r_in, float g_in, float b_in)
	{
		x = x_in;
		y = y_in;
		r = r_in;
		g = g_in;
		b = b_in;
	}

	Point() {
		x = 0;
		y = 0;
		r = 0;
		g = 0;
		b = 0;
	}
};


int main(int argc, char** argv) {
	//Create an array of three points (A triangle)
	Point pointArray[3];
	//Populate the array with user input
	std::cout << "Enter 3 points (enter a point as x,y:r,g,b):"<<std::endl;
	scanf("%f,%f:%f,%f,%f", &pointArray[0].x, &pointArray[0].y, &pointArray[0].r, &pointArray[0].g, &pointArray[0].b);
	scanf("%f,%f:%f,%f,%f", &pointArray[1].x, &pointArray[1].y, &pointArray[1].r, &pointArray[1].g, &pointArray[1].b);
	scanf("%f,%f:%f,%f,%f", &pointArray[2].x, &pointArray[2].y, &pointArray[2].r, &pointArray[2].g, &pointArray[2].b);
	cout << "You entered:" << endl << pointArray[0].x << ", " << pointArray[0].y << " : " << pointArray[0].r << ", " << pointArray[0].g << ", " << pointArray[0].b << endl;
	cout << pointArray[1].x << ", " << pointArray[1].y << " : " << pointArray[1].r << ", " << pointArray[1].g << ", " << pointArray[1].b << endl;
	cout << pointArray[2].x << ", " << pointArray[2].y << " : " << pointArray[2].r << ", " << pointArray[2].g << ", " << pointArray[2].b << endl;

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image(640, 480);

	//Part One	-------------------------------------------------------
	int minY = 480;
	int maxY = 0;
	int minX = 640;
	int maxX = 0;

	//Find bounds for triangle
	for (int i = 0; i < 3; i++) {
		if (pointArray[i].x < minX) {
			minX = pointArray[i].x;
		}
		if (pointArray[i].y < minY) {
			minY = pointArray[i].y;
		}
		if (pointArray[i].x > maxX) {
			maxX = pointArray[i].x;
		}
		if (pointArray[i].y > maxY) {
			maxY = pointArray[i].y;
		}
	}
	rgb_t white = make_colour(255, 255, 255);
	//Replace all points inside bounds with white
	/*for (int y = 0; y < 480; y++) {
		for (int x = 0; x < 640; x++) {
			if (x > minX && x < maxX && y > minY && y < maxY) {
				image.set_pixel(x, y, white);
			}
		}
	}*/
	//End Part One ----------------------------------------------------

	//Part Two ---------------------------------------------------------
	//Create weight variables (alpha, beta, gamma)
	float Weight_v1, Weight_v2, Weight_v3;
	//for (int y = 0; y < 480; y++) {
	//	for (int x = 0; x < 640; x++) {
	//		//Calculate weights
	//		Weight_v1 = ((pointArray[1].y - pointArray[2].y) * (x-pointArray[2].x) + (pointArray[2].x-pointArray[1].x) * (y-pointArray[2].y)) / ((pointArray[1].y - pointArray[2].y) * (pointArray[0].x - pointArray[2].x) + (pointArray[2].x - pointArray[1].x) * (pointArray[0].y - pointArray[2].y));
	//		Weight_v2 = ((pointArray[2].y - pointArray[0].y) * (x - pointArray[2].x) + (pointArray[0].x - pointArray[2].x) * (y - pointArray[2].y)) / ((pointArray[1].y - pointArray[2].y) * (pointArray[0].x - pointArray[2].x) + (pointArray[2].x - pointArray[1].x) * (pointArray[0].y - pointArray[2].y));
	//		Weight_v3 = 1 - Weight_v1 - Weight_v2;
	//		//Set pixels in triangle to white
	//		if (Weight_v1 >= 0 && Weight_v2 >= 0 && Weight_v3 >= 0) {
	//			image.set_pixel(x, y, white);
	//		}
	//	}
	//}
	//End Part Two --------------------------------------------------------

	//Part Three --------------------------------------------------------
	for (int y = 0; y < 480; y++) {
		for (int x = 0; x < 640; x++) {
			Weight_v1 = ((pointArray[1].y - pointArray[2].y) * (x - pointArray[2].x) + (pointArray[2].x - pointArray[1].x) * (y - pointArray[2].y)) / ((pointArray[1].y - pointArray[2].y) * (pointArray[0].x - pointArray[2].x) + (pointArray[2].x - pointArray[1].x) * (pointArray[0].y - pointArray[2].y));
			Weight_v2 = ((pointArray[2].y - pointArray[0].y) * (x - pointArray[2].x) + (pointArray[0].x - pointArray[2].x) * (y - pointArray[2].y)) / ((pointArray[1].y - pointArray[2].y) * (pointArray[0].x - pointArray[2].x) + (pointArray[2].x - pointArray[1].x) * (pointArray[0].y - pointArray[2].y));
			Weight_v3 = 1 - Weight_v1 - Weight_v2;
			if (Weight_v1 >= 0 && Weight_v2 >= 0 && Weight_v3 >= 0) {
				//Apply weights to each vertex color and set into triangle
				int red, green, blue;
				red = (Weight_v1 * pointArray[0].r + Weight_v2 * pointArray[1].r + Weight_v3 * pointArray[2].r) * 255;
				green = (Weight_v1 * pointArray[0].g + Weight_v2 * pointArray[1].g + Weight_v3 * pointArray[2].g) * 255;
				blue = (Weight_v1 * pointArray[0].b + Weight_v2 * pointArray[1].b + Weight_v3 * pointArray[2].b) * 255;
				rgb_t color = make_colour(red, green, blue);
				image.set_pixel(x, y, color);
			}
		}
	}
	//End Part Three -----------------------------------------------------

    image.save_image("triangle.bmp");
    std::cout << "Success" << std::endl;
}
