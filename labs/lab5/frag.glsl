#version 330 core
in vec3 ourColor;
in vec3 ourNormal;
in vec3 ourPosition;

uniform vec3 lightPos;
uniform mat4 camera;
uniform float specularity;

out vec4 fragColor;

void main() {
    fragColor = vec4(ourColor, 1.0f);
}
