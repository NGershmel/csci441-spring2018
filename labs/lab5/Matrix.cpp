//
// Created by Noah Gershmel on 2/12/18.
//
#pragma once
#include <iostream>
#include <cmath>

//Needed for certain projection matrices
class Vector3{
public:
    float x,y,z;

    Vector3(){
        x = 1;
        y = 1;
        z = 1;
    }
    Vector3(float x_in, float y_in, float z_in){
        x = x_in;
        y = y_in;
        z = z_in;
    }

    Vector3 operator*(Vector3 crossVector){
        Vector3 resuyltVector = Vector3();
        resuyltVector.x = y*crossVector.z - z*crossVector.y;
        resuyltVector.y = z*crossVector.x - x*crossVector.z;
        resuyltVector.z = x*crossVector.y - y*crossVector.x;
        return resuyltVector;
    }

    Vector3 normalize(){
        Vector3 resultVector = Vector3();
        float xyMag = sqrt((x*x) + (y * y));
        float magnitude = sqrt((xyMag*xyMag) + (z * z));
        resultVector.x = x / magnitude;
        resultVector.y = y / magnitude;
        resultVector.z = z / magnitude;
        return resultVector;
    }

    void print(){
        std::cout << x << ", " << y << ", " << z << std::endl;
    }
};

//A class to store matrices, manipulate matrices, and auto generate matrices from parameters
class Matrix3 {

public:
    //Actual values of the 4x4 matrix
    float values[3][3] = {{1,0,0},{0,1,0},{0,0,1}};

    //Standard constructor
    Matrix3(){
        values[0][0] = 1;
        values[1][1] = 1;
        values[2][2] = 1;
    }

    //Returns a scale matrix based on parameters
    static Matrix3 scale(float xScale, float yScale){
        Matrix3 scaleMatrix = Matrix3();
        scaleMatrix.values[0][0] = xScale;
        scaleMatrix.values[1][1] = yScale;
        scaleMatrix.values[2][2] = 1;
        return scaleMatrix;
    }

    static Matrix3 rotateCCW(float radianRotate){
        Matrix3 newMatrix = Matrix3();
        newMatrix.values[0][0] = cos(radianRotate);
        newMatrix.values[0][1] = -sin(radianRotate);
        newMatrix.values[1][0] = sin(radianRotate);
        newMatrix.values[1][1] = cos(radianRotate);
        newMatrix.values[2][2] = 1;
        return newMatrix;
    }

    static Matrix3 rotateCW(float radianRotate){
        Matrix3 rotateMatrix = Matrix3();
        rotateMatrix.values[0][0] = cos(radianRotate);
        rotateMatrix.values[0][1] = sin(radianRotate);
        rotateMatrix.values[1][0] = -sin(radianRotate);
        rotateMatrix.values[1][1] = cos(radianRotate);
        rotateMatrix.values[2][2] = 1;
        return  rotateMatrix;
    }

    //Returns a translation matrix from the parameters
    static Matrix3 translate(float xtranslate, float ytranslate){
        Matrix3 translateMatrix = Matrix3();
        translateMatrix.values[0][2] = xtranslate;
        translateMatrix.values[1][2] = ytranslate;
        translateMatrix.values[2][2] = 1;
        return translateMatrix;
    }

    //Generates an orthographic projection matrix
    static Matrix3 OrthographicProjection(float r, float l, float t, float b, float n, float f){
        Matrix3 viewMat = Matrix3();
        viewMat.values[0][0] = 2/(r-l);
        viewMat.values[1][1] = 2/(t-b);
        viewMat.values[2][2] = -2/(-n+f);
        viewMat.values[0][3] = -(r + l)/(r-l);
        viewMat.values[1][3] = -(t+b)/(t-b);
        viewMat.values[2][3] = -(n+f)/(-n+f);
        return viewMat;
    }

    //Generates a perspective projection matrix
    static Matrix3 PerspectiveProjection(float n, float f){
        Matrix3 perspective = Matrix3();
        perspective.values[0][0] = n;
        perspective.values[1][1] = n;
        perspective.values[2][2] = n+f;
        perspective.values[2][3] = -(f*n);
        perspective.values[3][2] = -1;
        return perspective;
    }

    //Adds to matrices together
    Matrix3 operator+(Matrix3 added){
        Matrix3 newMatrix = Matrix3();
        for (int row=0; row<3; row++){
            for (int col=0; col<3; col++){
                newMatrix.values[row][col] = this->values[row][col] + added.values[row][col];
            }
        }
        return newMatrix;
    }

    //Multiples two matrices together
    Matrix3 operator*(Matrix3 multiplied){

        Matrix3 newMatrix = Matrix3();
        for (int row = 0; row<3; row++){
            for (int col= 0; col<3; col++){
                float temp = 0.0;
                for (int cval = 0; cval<3; cval++) {
                    temp += (values[row][cval] * multiplied.values[cval][col]);
                }
                newMatrix.values[row][col] = temp;
            }
        }
        return newMatrix;
    }

    //Multiplies a matrix by a scalar
    Matrix3 operator*(float scalar){
        Matrix3 newMatrix = Matrix3();
        for (int row=0; row<3; row++){
            for (int col=0; col<3; col++){
                newMatrix.values[row][col] = values[row][col] * scalar;
            }
        }
        return newMatrix;
    }

    //Generates a world space to camera space transformation matrix
    static Matrix3 worldToCamera(Matrix3 cameraLoc){
        Vector3 g = Vector3(-cameraLoc.values[0][2], -cameraLoc.values[1][2], -cameraLoc.values[2][2]);
        Vector3 t = Vector3(cameraLoc.values[0][1], cameraLoc.values[1][1], cameraLoc.values[2][1]);
        Vector3 w = Vector3(-g.normalize().x, -g.normalize().y, -g.normalize().z);
        Vector3 u = (t*w).normalize();
        Vector3 v = (w*u);
        Matrix3 cameraRotate = Matrix3();
        cameraRotate.values[0][0] = u.x;
        cameraRotate.values[1][0] = u.y;
        cameraRotate.values[2][0] = u.z;
        cameraRotate.values[0][1] = v.x;
        cameraRotate.values[1][1] = v.y;
        cameraRotate.values[2][1] = v.z;
        cameraRotate.values[0][2] = w.x;
        cameraRotate.values[1][2] = w.y;
        cameraRotate.values[2][2] = w.z;
        cameraRotate.values[3][3] = 1;

        //
        Matrix3 cameraTranslate = Matrix3();
        cameraTranslate.values[0][3] = -(cameraLoc.values[0][3]);
        cameraTranslate.values[1][3] = -(cameraLoc.values[1][3]);
        cameraTranslate.values[2][3] = -(cameraLoc.values[2][3]);

        return cameraTranslate * cameraRotate;
    }

    //Generates a viewport transformation matrix
    static Matrix3 viewportTransform(float nx, float ny){
        Matrix3 viewportMatrix = Matrix3();
        viewportMatrix.values[0][0] = (nx/2);
        viewportMatrix.values[1][1] = (ny/2);
        viewportMatrix.values[0][3] = (nx-1)/2;
        viewportMatrix.values[1][3] = (ny-1)/2;
        return viewportMatrix;
    }

    //Prints the matrix
    static void print(Matrix3 printMatrix){
        std::cout << std::endl << "----------" << std::endl;
        std::cout << printMatrix.values[0][0] << ",\t" << printMatrix.values[0][1] << ",\t" << printMatrix.values[0][2] << std::endl;
        std::cout << printMatrix.values[1][0] << ",\t" << printMatrix.values[1][1] << ",\t" << printMatrix.values[1][2] << std::endl;
        std::cout << printMatrix.values[2][0] << ",\t" << printMatrix.values[2][1] << ",\t" << printMatrix.values[2][2] << std::endl;
        std::cout << "----------" << std::endl;
    }
};