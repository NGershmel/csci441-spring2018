#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "Matrix.cpp"

const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 480;
float aspectRatio = SCREEN_WIDTH/SCREEN_HEIGHT;
bool perspective = false;
bool cyl = false;
Matrix cameraPos = Matrix();
Matrix cameraRot = Matrix();
Matrix cameraLoc = Matrix();
Matrix modelTranslate = Matrix();
Matrix modelRotation = Matrix();
Matrix modelScale = Matrix();
Matrix modelTransform = Matrix();
Matrix vp = Matrix();
Matrix cam = Matrix();
Matrix proj = Matrix();

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window, Shader &shader) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

//Moves the camera up in the y dierection and tilts it to continue looking at the origin
void moveCameraUp(){
    cameraPos = cameraPos * Matrix::Translate(0,0.1,0);
    cameraRot = cameraRot * Matrix::RotateX((float) (3.14149/8)/20);
    cameraLoc = cameraRot * cameraPos;
}

//Moves the camer ain the negative y direction and tilts it to continue looking at the origin
void moveCameraDown(){
    cameraPos = cameraPos * Matrix::Translate(0,-0.1f,0);
    cameraRot = cameraRot * Matrix::RotateX((float) -(3.14149/8)/20);
    cameraLoc = cameraPos * cameraRot;
}

//Returns a slightly scaled down matrix
Matrix scaleDown(){
    return Matrix::Scale(0.9,0.9,0.9);
}

//Returns a slightly scaled up matrix
Matrix scaleUp(){
    return Matrix::Scale(1.1,1.1,1.1);
}

//Returns a slightly translated matrix in the x positive direction
Matrix moveXPos(){
    return Matrix::Translate(0.1,0,0);
}

//Returns a slightly translated matrix in the x negative direction
Matrix moveXNeg(){
    return Matrix::Translate(-0.1f,0,0);
}

//Returns a slightly translated matrix in the y positive direction
Matrix moveYPos(){
    return Matrix::Translate(0,0.1,0);
}

//Returns a slightly translated matrix in the y negative direction
Matrix moveYNeg(){
    return Matrix::Translate(0,-0.1f,0);
}

//Returns a slightly translated matrix in the z positive direction
Matrix moveZPos(){
    return Matrix::Translate(0,0,0.1);
}

//Returns a slightly translated matrix in the z negative direction
Matrix moveZNeg(){
    return Matrix::Translate(0,0,-0.1f);
}

//Returns a rotation matrix about the Y axis
Matrix rotateYPos(){
    return Matrix::RotateY(0.05f);
}

//Returns a rotation matrix about the Y axis in the negative direction
Matrix rotateYNeg(){
    return Matrix::RotateY(-0.05f);
}

//Returns a rotation matrix about the X axis
Matrix rotateXPos(){
    return Matrix::RotateX(0.05f);
}

//Returns a rotation matrix about the X axis in the negative direction
Matrix rotateXNeg(){
    return Matrix::RotateX(-0.05f);
}

//Returns a rotation matrix about the Z axis
Matrix rotateZPos(){
    return Matrix::RotateZ(0.05f);
}

//Returns a rotation matrix about the Z axis in the negative direction
Matrix rotateZNeg(){
    return Matrix::RotateZ(-0.05f);
}

//Handles all the special inputs for this program
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    //Swap between projection modes on slash press
    if (key == GLFW_KEY_SLASH && action == GLFW_RELEASE) {
        perspective = !perspective;
    }

    //Swap between primitives on space press
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) {
        cyl = !cyl;
    }

    //Move camera on w and s press and holds
    if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        moveCameraUp();
    } else if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        moveCameraDown();
    }

    //Scale primitive on - and = press or holds
    if (key == GLFW_KEY_MINUS && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelScale = modelScale * scaleDown();
    } else if (key == GLFW_KEY_EQUAL && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelScale = modelScale * scaleUp();
    }

    //Move the primitive on arrow key press or holds
    if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelTranslate = modelTranslate * moveXNeg();
    } else if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelTranslate = modelTranslate * moveXPos();
    }
    if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelTranslate = modelTranslate * moveYPos();
    } else if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelTranslate = modelTranslate * moveYNeg();
    }
    if (key == GLFW_KEY_COMMA && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelTranslate = modelTranslate * moveZNeg();
    } else if (key == GLFW_KEY_PERIOD && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelTranslate = modelTranslate * moveZPos();
    }

    //Rotate the primitive on u, i, o, p, [, and ] press and holds
    if (key == GLFW_KEY_U && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelRotation = modelRotation * rotateXNeg();
    } else if (key == GLFW_KEY_I && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelRotation = modelRotation * rotateXPos();
    }
    if (key == GLFW_KEY_O && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelRotation = modelRotation * rotateYNeg();
    } else if (key == GLFW_KEY_P && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelRotation = modelRotation * rotateYPos();
    }
    if (key == GLFW_KEY_LEFT_BRACKET && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
        modelRotation = modelRotation * rotateZNeg();
    } else if (key == GLFW_KEY_RIGHT_BRACKET && (action == GLFW_PRESS || action == GLFW_REPEAT)){
        modelRotation = modelRotation * rotateZPos();
    }
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//Returns a projection matrix based on orthographic or perspective projection
Matrix getProjection(){
    if (perspective) {
        return Matrix::OrthographicProjection(SCREEN_WIDTH,0,SCREEN_HEIGHT,-0,-20,10) * Matrix::PerspectiveProjection(0.5,8);
    } else {
        return Matrix::OrthographicProjection(SCREEN_WIDTH,0,SCREEN_HEIGHT,0,-20,10);
    }
}

int main(void) {
    //Initialize the camera position
    cameraLoc = Matrix::Translate(0,0,-20);
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }


    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Lab 4", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the model */
    float vertices[] = {
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 0.0f, 1.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 0.0f, 1.0f,

        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  1.0f, 0.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  1.0f, 0.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  1.0f, 0.0f, 0.0f,

        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f, -0.5f, -0.5f,  0.0f, 1.0f, 0.0f,

        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
         0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f,  0.5f,  0.0f, 1.0f, 0.0f,
        -0.5f,  0.5f, -0.5f,  0.0f, 1.0f, 0.0f
    };


    //Sets the information for a cylinder
    int resolution = 20;
    float radius = 0.5;
    float depth = 1;
    float angle = (2*3.14159)/resolution;
    //Creates an array to hold the cylinder
    float cylinderVertices[resolution*36*2];
    //Creates the sides
    for (int i=0; i<resolution; i++){
        cylinderVertices[i*36] = radius * cos(angle*i);
        cylinderVertices[i*36+1] = depth/2;
        cylinderVertices[i*36+2] = radius * sin(angle*i);
        cylinderVertices[i*36+3] = 0.75;
        cylinderVertices[i*36+4] = 0.25;
        cylinderVertices[i*36+5] = 0.25;

        cylinderVertices[i*36+6] = radius * cos(angle*i);
        cylinderVertices[i*36+7] = -depth/2;
        cylinderVertices[i*36+8] = radius * sin(angle*i);
        cylinderVertices[i*36+9] = 0.75;
        cylinderVertices[i*36+10] = 0.25;
        cylinderVertices[i*36+11] = 0.25;

        cylinderVertices[i*36+12] = radius * cos(angle*i + angle);
        cylinderVertices[i*36+13] = depth/2;
        cylinderVertices[i*36+14] = radius * sin(angle*i + angle);
        cylinderVertices[i*36+15] = 0.25;
        cylinderVertices[i*36+16] = 0.25;
        cylinderVertices[i*36+17] = 0.75;

        cylinderVertices[i*36+18] = radius * cos(angle*i);
        cylinderVertices[i*36+19] = -depth/2;
        cylinderVertices[i*36+20] = radius * sin(angle*i);
        cylinderVertices[i*36+21] = 0.75;
        cylinderVertices[i*36+22] = 0.25;
        cylinderVertices[i*36+23] = 0.25;

        cylinderVertices[i*36+24] = radius * cos(angle*i + angle);
        cylinderVertices[i*36+25] = depth/2;
        cylinderVertices[i*36+26] = radius * sin(angle*i + angle);
        cylinderVertices[i*36+27] = 0.25;
        cylinderVertices[i*36+28] = 0.25;
        cylinderVertices[i*36+29] = 0.75;

        cylinderVertices[i*36+30] = radius * cos(angle*i + angle);
        cylinderVertices[i*36+31] = -depth/2;
        cylinderVertices[i*36+32] = radius * sin(angle*i + angle);
        cylinderVertices[i*36+33] = 0.25;
        cylinderVertices[i*36+34] = 0.25;
        cylinderVertices[i*36+35] = 0.75;
    }
    //Creates the top and bottom
    for (int i=resolution; i<resolution*2; i++){
        cylinderVertices[i*36] = radius * cos(angle*i);
        cylinderVertices[i*36+1] = depth/2;
        cylinderVertices[i*36+2] = radius * sin(angle*i);
        cylinderVertices[i*36+3] = 0.75;
        cylinderVertices[i*36+4] = 0.25;
        cylinderVertices[i*36+5] = 0.25;

        cylinderVertices[i*36+6] = 0;
        cylinderVertices[i*36+7] = depth/2;
        cylinderVertices[i*36+8] = 0;
        cylinderVertices[i*36+9] = 0.75;
        cylinderVertices[i*36+10] = 0.75;
        cylinderVertices[i*36+11] = 0.75;

        cylinderVertices[i*36+12] = radius * cos(angle*i + angle);
        cylinderVertices[i*36+13] = depth/2;
        cylinderVertices[i*36+14] = radius * sin(angle*i + angle);
        cylinderVertices[i*36+15] = 0.25;
        cylinderVertices[i*36+16] = 0.25;
        cylinderVertices[i*36+17] = 0.75;

        cylinderVertices[i*36+18] = radius * cos(angle*i);
        cylinderVertices[i*36+19] = -depth/2;
        cylinderVertices[i*36+20] = radius * sin(angle*i);
        cylinderVertices[i*36+21] = 0.75;
        cylinderVertices[i*36+22] = 0.25;
        cylinderVertices[i*36+23] = 0.25;

        cylinderVertices[i*36+24] = 0;
        cylinderVertices[i*36+25] = -depth/2;
        cylinderVertices[i*36+26] = 0;
        cylinderVertices[i*36+27] = 0.75;
        cylinderVertices[i*36+28] = 0.75;
        cylinderVertices[i*36+29] = 0.75;

        cylinderVertices[i*36+30] = radius * cos(angle*i + angle);
        cylinderVertices[i*36+31] = -depth/2;
        cylinderVertices[i*36+32] = radius * sin(angle*i + angle);
        cylinderVertices[i*36+33] = 0.25;
        cylinderVertices[i*36+34] = 0.25;
        cylinderVertices[i*36+35] = 0.75;
    }

    // copy vertex data
    GLuint VBO[2];
    glGenBuffers(2, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    //glBufferData(GL_ARRAY_BUFFER, sizeof(cylinderVertices), cylinderVertices, GL_STATIC_DRAW);


    // describe vertex layout
    GLuint VAO;
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6*sizeof(float),
            (void*)(3*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    moveCameraUp();
    moveCameraDown();

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        int transformLocation = glGetUniformLocation(shader.id(), "transformMat");
        int viewMatrixLocation = glGetUniformLocation(shader.id(), "viewMat");
        int projectionLocation = glGetUniformLocation(shader.id(), "projMat");
        // process input
        processInput(window, shader);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // activate shader
        shader.use();

        //Updates the model matrix
        modelTransform = modelScale * modelRotation * modelTranslate;

        //Updates the viewport matrix (based on camera position)
        vp = Matrix::viewportTransform(640, 480) * Matrix::worldToCamera(cameraLoc);

        //Updates the projection matrix
        proj = getProjection();

        //Passes in the updated uniforms
        glUniformMatrix4fv(transformLocation, 1, GL_FALSE, &modelTransform.values[0][0]);
        glUniformMatrix4fv(viewMatrixLocation, 1, GL_FALSE, &vp.values[0][0]);
        glUniformMatrix4fv(projectionLocation, 1, GL_FALSE, &proj.values[0][0]);

        //Binds the correct primitive
        if (cyl) {
            glBufferData(GL_ARRAY_BUFFER, sizeof(cylinderVertices), cylinderVertices, GL_STATIC_DRAW) ;
        } else {
            glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
        }

        // render the cube
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(vertices));

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
