#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aColor;

out vec3 ourColor;

uniform mat4 transformMat;
uniform mat4 viewMat;
uniform mat4 projMat;

void main() {
    vec4 newVec = vec4(aPos, 1.0);
    gl_Position = newVec * transformMat * viewMat * projMat;
    ourColor = aColor;
}
