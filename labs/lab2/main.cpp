#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <stdio.h>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#define GLFW_VERSION_MAJOR 3
#define GLFW_VERSION_MINOR 3

int w_width, w_height;
struct Point {
	float x, y, z, r, g, b;
};
/**
 * BELOW IS A BUNCH OF HELPER CODE
 * You do not need to understand what is going on with it, but if you want to
 * know, let me know and I can walk you through it.
 */

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
	w_width = width;
	w_height = height;
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

GLFWwindow* initWindow() {
    GLFWwindow* window;
    if (!glfwInit()) {
		std::cout << "FAIL glfwInit" << std::endl;
        return NULL;
    }

    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(640, 480, "Noah Gershmel - Lab 2", NULL, NULL);
	w_width = 640;
	w_height = 480;

    if (!window) {
        glfwTerminate();
        return NULL;
    }

    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return NULL;
    }

    return window;
}

std::string shaderTypeName(GLenum shaderType) {
    switch(shaderType) {
        case GL_VERTEX_SHADER: return "VERTEX";
        case GL_FRAGMENT_SHADER: return "FRAGMENT";
        default: return "UNKNOWN";
    }
}

std::string readFile(const std::string& fileName) {
    std::ifstream stream(fileName);
    std::stringstream buffer;
    buffer << stream.rdbuf();

    std::string source = buffer.str();
    std::cout << "Source:" << std::endl;
    std::cout << source << std::endl;

    return source;
}

/** END OF CODE THAT YOU DON'T NEED TO WORRY ABOUT */

GLuint createShader(const std::string& fileName, GLenum shaderType) {
    std::string source = readFile(fileName);
    const char* src_ptr = source.c_str();

    GLuint shader;
	shader = glCreateShader(shaderType);
	glShaderSource(shader, 1, &src_ptr, NULL);
	glCompileShader(shader);
    // create the shader using
    // glCreateShader, glShaderSource, and glCompileShader

    // Perform some simple error handling on the shader
    int success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success) {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cerr << "ERROR::SHADER::" << shaderTypeName(shaderType)
            <<"::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return shader;
}

GLuint createShaderProgram(GLuint vertexShader, GLuint fragmentShader) {
    // create the program using glCreateProgram, glAttachShader, glLinkProgram
    GLuint program;
	program = glCreateProgram();
	glAttachShader(program, vertexShader);
	glAttachShader(program, fragmentShader);
	glLinkProgram(program);

    // Perform some simple error handling
    int success;
    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        char infoLog[512];
        glGetProgramInfoLog(program, 512, NULL, infoLog);
        std::cerr << "ERROR::PROGRAM::COMPILATION_FAILED\n"
            << infoLog << std::endl;
    }

    return program;
}

int main(void) {
    GLFWwindow* window = initWindow();
    if (!window) {
        std::cout << "There was an error setting up the window" << std::endl;
        return 1;
    }
	
    /** YOU WILL ADD DATA INITIALIZATION CODE STARTING HERE */
	Point pointArray[3];
	std::cout << "Enter 3 points (enter a point as x,y,z:r,g,b):" << std::endl;
	scanf("%f,%f,%f:%f,%f,%f", &pointArray[0].x, &pointArray[0].y, &pointArray[0].z, &pointArray[0].r, &pointArray[0].g, &pointArray[0].b);
	scanf("%f,%f,%f:%f,%f,%f", &pointArray[1].x, &pointArray[1].y, &pointArray[1].z, &pointArray[1].r, &pointArray[1].g, &pointArray[1].b);
	scanf("%f,%f,%f:%f,%f,%f", &pointArray[2].x, &pointArray[2].y, &pointArray[2].z, &pointArray[2].r, &pointArray[2].g, &pointArray[2].b);
    /* PART1: ask the user for coordinates and colors, and convert to normalized
     * device coordinates */
	printf("%d,%d\n", w_width, w_height);
	for (int i = 0; i < 3; i++) {
		pointArray[i].x = (pointArray[i].x * (2/float(w_width))) - 1;
		pointArray[i].y = 1-(pointArray[i].y * (2/float(w_height)));		
		pointArray[i].z = 0;
	}
	std::cout << "Your normalized coordinated are: " << std::endl;
	std::cout << pointArray[0].x << ", " << pointArray[0].y << " : " << pointArray[0].r << ", " << pointArray[0].g << ", " << pointArray[0].b << std::endl;
	std::cout << pointArray[1].x << ", " << pointArray[1].y << " : " << pointArray[1].r << ", " << pointArray[1].g << ", " << pointArray[1].b << std::endl;
	std::cout << pointArray[2].x << ", " << pointArray[2].y << " : " << pointArray[2].r << ", " << pointArray[2].g << ", " << pointArray[2].b << std::endl;
	
	float triangle[] = { pointArray[0].x, pointArray[0].y, pointArray[0].z, pointArray[0].r, pointArray[0].g, pointArray[0].b,
						  pointArray[1].x, pointArray[1].y, pointArray[1].z, pointArray[1].r, pointArray[1].g, pointArray[1].b,
						  pointArray[2].x, pointArray[2].y, pointArray[2].z, pointArray[2].r, pointArray[2].g, pointArray[2].b };

	std::cout << "Attempting part 2" << std::endl;
    /** PART2: map the data */	
	GLuint VBO, VAO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	std::cout << "Created VAO" << std::endl;
	glGenBuffers(1,&VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);
	std::cout << "Created VBO" << std::endl;
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
	glEnableVertexAttribArray(1);
	std::cout << "Created attribute array" << std::endl;

	
    /** PART3: create the shader program */

    // create the shaders
    GLuint vertexShader = createShader("../vert.glsl", GL_VERTEX_SHADER);
    GLuint fragmentShader = createShader("../frag.glsl", GL_FRAGMENT_SHADER);
	std::cout << "Created shaders" << std::endl;
    // create the shader program
    GLuint shaderProgram = createShaderProgram(vertexShader, fragmentShader);
	std::cout << "Created shader program" << std::endl;
	glDeleteShader(vertexShader);
	glDeleteShader(fragmentShader);

    /** END INITIALIZATION CODE */

    while (!glfwWindowShouldClose(window)) {
        processInput(window);

        /** PART4: Implemting the rendering loop */

        // clear the screen with your favorite color using glClearColor
		glClearColor(0.2f, 0.2f, 0.2f, 0.8f);
        glClear(GL_COLOR_BUFFER_BIT);
		
		
        // set the shader program using glUseProgram
		glUseProgram(shaderProgram);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);
        /** END RENDERING CODE */

        // Swap front and back buffers
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
	
    glfwTerminate();
    return 0;
}
