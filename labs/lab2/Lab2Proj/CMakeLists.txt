cmake_minimum_required(VERSION 3.8)
project(Lab2Proj)

set(CMAKE_CXX_STANDARD 11)

set(SOURCE_FILES main.cpp)
add_executable(Lab2Proj ${SOURCE_FILES})