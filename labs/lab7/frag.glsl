#version 330 core

out vec4 fragColor;

in vec2 ourTexCoord;

uniform sampler2D texSampler;

/**
 * TODO: PART-1 update the fragment shader to get the texture coordinates from
 * the vertex shader
 */

/**
 * TODO: PART-3 update the fragment shader to get the fragColor color from the
 * texture, and add the sampler2D.
 */

void main() {
    //fragColor = vec4(1, 0, 0, 1);
    //fragColor = vec4(ourTexCoord.x, ourTexCoord.y, 0, 1);
    fragColor = texture(texSampler, ourTexCoord);
}
