//
// Created by Noah Gershmel on 2/12/18.
//
#include <vector>
#include "Matrix.cpp"

//This structure holds an individual location, rotation, and scale for an object
struct KeyFrame {
    Matrix targetPos;
    Matrix targetScale;
    Matrix targetRotation;
    float targetTime;
};

//Enumeration for different types of interpolation (currently only linear works)
enum InterpolationType {linear, smooth};

//Class to control an animation, built up with a vector of key frames
class Animation {
private:
    //Necessary variables for an animation
    KeyFrame firstFrame;
    InterpolationType interpolationType;
    std::vector<KeyFrame> frames;
    int frame_iter = 1;
    float start_global_time = 0;
    float animation_length;
public:
    //Default constructor
    Animation(){
        firstFrame = KeyFrame();
        interpolationType = linear;
        animation_length = 0;
    }

    //Constructor to build a generic animation
    Animation(KeyFrame startFrame, float length, int num_frames, InterpolationType type){
        firstFrame = startFrame;
        if (startFrame.targetTime != 0){
            std::cout << "ANIMATION MUST START AT TIME 0" << std::endl;
        }
        interpolationType = type;
        animation_length = length;
        frames.resize(num_frames);
        frames[0] = firstFrame;
    }

    //Appends a frame onto the animation
    void addFrame(KeyFrame addFrame){
        frames[frame_iter] = addFrame;
        frame_iter++;
        //frames = sort(frames);
    }

    //Begins running the animation
    void startAnimation(float startTime){
        if (start_global_time == 0) {
            start_global_time = startTime;
        }
    }

    //Gets the transformation matrix from the animation at the current time
    Matrix getTransform(double currentTime){
        if (interpolationType == linear) {
            return getLinearInterpolation(fmod(currentTime-start_global_time, double(animation_length)));
        } else if (interpolationType == smooth) {
            return  getSmoothInterpolation(fmod(currentTime-start_global_time, double(animation_length)));
        }
    }

    //Interpolates all the values linearly
    Matrix getLinearInterpolation(double currentTime){
        KeyFrame frameOne = getFramePrior(currentTime);
        KeyFrame frameTwo = getFramePost(frameOne.targetTime);
        float timePercentage = (currentTime - frameOne.targetTime) / frameTwo.targetTime;
        Matrix currentPos = interpolateMatrix(frameOne.targetPos, frameTwo.targetPos, timePercentage);
        Matrix currentScale = interpolateMatrix(frameOne.targetScale, frameTwo.targetScale, timePercentage);
        Matrix currentRotation = interpolateMatrix(frameOne.targetRotation, frameTwo.targetRotation, timePercentage);
        
        return currentPos * currentScale * currentRotation;
    }

    //Interprets all the values in a curve (not yet working)
    Matrix getSmoothInterpolation(double currentTime){
        //TODO use a smoother interpolation
        return getLinearInterpolation(currentTime);
    }

    //Gets the first frame to interpolate based on time
    KeyFrame getFramePrior(double time){
        for (int i=0; i<frames.size(); i++){
            if (frames[i].targetTime > time){
                return frames[i-1];
            }
        }
        return frames[frames.size()-1];
    }

    //Gets the second frame to interpolate based on time
    KeyFrame getFramePost(double priorFrameTime){
        for (int i=0; i<frames.size(); i++){
            if (frames[i].targetTime > priorFrameTime){
                return frames[i];
            }
        }
        return frames[frames.size()-1];
    }

    //Interpolates a matrix linearly
    Matrix interpolateMatrix(Matrix startMatrix, Matrix endMatrix, float t){
        Matrix interpolatedMatrix = Matrix();
        for (int row=0; row<3; row++) {
            for (int col=0; col<3; col++) {
                interpolatedMatrix.values[row][col] = startMatrix.values[row][col] + (endMatrix.values[row][col] - startMatrix.values[row][col]) * t;
            }
        }
        return interpolatedMatrix;
    }
};
