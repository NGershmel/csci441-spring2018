//
// Created by Noah Gershmel on 2/12/18.
//
#pragma once
#include <iostream>
#include <cmath>

class Matrix {
private:
    int rows, columns;

public:
    float values[3][3] = {{0,0,0},{0,0,0},{0,0,0}};

    Matrix(int rows, int columns){
        this->rows = rows;
        this->columns = columns;
    }

    Matrix(){
        values[0][0] = 1;
        values[1][1] = 1;
        values[2][2] = 1;
    }

    static Matrix Scale(float xScale, float yScale){
        Matrix scaleMatrix(3,3);
        scaleMatrix.values[0][0] = xScale;
        scaleMatrix.values[1][1] = yScale;
        scaleMatrix.values[2][2] = 1;
        return scaleMatrix;
    }

    static Matrix RotateCCW(float radianRotate){
        Matrix newMatrix(3,3);
        newMatrix.values[0][0] = cos(radianRotate);
        newMatrix.values[0][1] = -sin(radianRotate);
        newMatrix.values[1][0] = sin(radianRotate);
        newMatrix.values[1][1] = cos(radianRotate);
        newMatrix.values[2][2] = 1;
        return newMatrix;
    }

    static Matrix RotateCW(float radianRotate){
        Matrix rotateMatrix(3,3);
        rotateMatrix.values[0][0] = cos(radianRotate);
        rotateMatrix.values[0][1] = sin(radianRotate);
        rotateMatrix.values[1][0] = -sin(radianRotate);
        rotateMatrix.values[1][1] = cos(radianRotate);
        rotateMatrix.values[2][2] = 1;
        return  rotateMatrix;
    }

    static Matrix Translate(float xtranslate, float ytranslate){
        Matrix translateMatrix = Matrix();
        translateMatrix.values[0][2] = xtranslate;
        translateMatrix.values[1][2] = ytranslate;
        translateMatrix.values[2][2] = 1;
        return translateMatrix;
    }

    Matrix operator+(Matrix added){
        Matrix newMatrix(3,3);
        for (int row=0; row<3; row++){
            for (int col=0; col<3; col++){
                newMatrix.values[row][col] = this->values[row][col] + added.values[row][col];
            }
        }
        return newMatrix;
    }

    Matrix operator*(Matrix multiplied){
        Matrix newMatrix(3,3);
        for (int row=0; row<3; row++){
            for (int col=0; col<3; col++){
                for (int cval =0; cval<3; cval++) {
                    newMatrix.values[row][col] += this->values[row][cval] * multiplied.values[cval][col];
                }
            }
        }
        return newMatrix;
    }

    Matrix operator*(float scalar){
        Matrix newMatrix = Matrix();
        for (int row=0; row<3; row++){
            for (int col=0; col<3; col++){
                newMatrix.values[row][col] = values[row][col] * scalar;
            }
        }
        return newMatrix;
    }

    static void print(Matrix printMatrix){
        std::cout << printMatrix.values[0][0] << ",\t" << printMatrix.values[0][1] << ",\t" << printMatrix.values[0][2] << std::endl;
        std::cout << printMatrix.values[1][0] << ",\t" << printMatrix.values[1][1] << ",\t" << printMatrix.values[1][2] << std::endl;
        std::cout << printMatrix.values[2][0] << ",\t" << printMatrix.values[2][1] << ",\t" << printMatrix.values[2][2] << std::endl;
    }

    static float** toArray(Matrix inMatrix){
        float** arrayForm = 0;
        arrayForm = new float*[3];
        for (int row=0; row<3; row++){
            arrayForm[row] = new float[3];
            for (int col=0; col<3; col++){
                arrayForm[row][col] = inMatrix.values[row][col];
            }
        }
        return arrayForm;
    }
};