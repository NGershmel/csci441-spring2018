#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

out vec3 myColor;

uniform mat3 transformMat;

void main() {
    vec3 twoVector = vec3(aPos, 1.0);
    twoVector = twoVector * transformMat;
    gl_Position = vec4(twoVector.x, twoVector.y, 0.0, 1.0);
    myColor = aColor;
}
