#include <iostream>
#include <sstream>
#include <fstream>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <vector>
#include "Matrix.cpp"
#include "Animation.cpp"

//global variables to control the animations
int animation = 0;
bool spaceEvent = false;
std::vector<Animation> animations(1);

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

void processInput(GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}

//Handles the space bar being released
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    if (key == GLFW_KEY_SPACE && action == GLFW_RELEASE) {
        animation++;
        if (animation>4) {
            animation=0;
        }
        spaceEvent = true;
    }
}

//Seven keyframes stored into an animation
void createAnimations(){
    KeyFrame frameOne;
    frameOne.targetScale = Matrix::Scale(0.5, 0.25);
    frameOne.targetPos = Matrix::Translate(0,-1);
    frameOne.targetRotation = Matrix::RotateCW(0);
    frameOne.targetTime = 0;

    KeyFrame frameOneN;
    frameOneN.targetScale = Matrix::Scale(0.25, 0.4);
    frameOneN.targetPos = Matrix::Translate(0,-0.85f);
    frameOneN.targetRotation = Matrix::RotateCW(0);
    frameOneN.targetTime = 0.4;

    KeyFrame frameTwo;
    frameTwo.targetScale = Matrix::Scale(0.3, 0.3);
    frameTwo.targetPos =  Matrix::Translate(0, 0.8);
    frameTwo.targetRotation = Matrix::RotateCW(0);
    frameTwo.targetTime = 0.7;

    KeyFrame frameThree;
    frameThree.targetScale = Matrix::Scale(0.3, 0.3);
    frameThree.targetPos = Matrix::Translate(0, 1);
    frameThree.targetRotation = Matrix::RotateCW(0);
    frameThree.targetTime = 1;


    Animation bounce = Animation(frameOne, 2, 7, linear);
    bounce.addFrame(frameOneN);
    bounce.addFrame(frameTwo);
    bounce.addFrame(frameThree);
    frameTwo.targetTime = 1.4;
    frameOne.targetTime = 2;
    frameOneN.targetTime = 1.6;
    bounce.addFrame(frameTwo);
    bounce.addFrame(frameOneN);
    bounce.addFrame(frameOne);

    animations[0] = bounce;
}

//Updates the transform matrix
Matrix getTransform(Matrix transform){
    Matrix newTransform;
    switch (animation){
        case 0:
            return Matrix();
        case 1:
            newTransform = Matrix::RotateCW(glfwGetTime());
            break;
        case 2:
            newTransform = Matrix::Scale(0.5,0.5) * Matrix::Translate(0.5,0.75) * Matrix::RotateCCW(glfwGetTime())  * Matrix::Translate(0.5,0.5);
            break;
        case 3:
            newTransform = Matrix::Scale(cos(glfwGetTime()), cos(glfwGetTime()));
            break;
        case 4:
            animations[0].startAnimation(glfwGetTime());
            newTransform = animations[0].getTransform(glfwGetTime());
            //impress him i.e. try to get keyframes working
            break;
        default:
            newTransform = Matrix();
    }
    return newTransform;
}

int main(void) {
    //Create the bounce animation
    createAnimations();
    /* Initialize the library */
    GLFWwindow* window;
    if (!glfwInit()) {
        return -1;
    }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);


    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(640, 480, "Lab 3", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    glfwSetKeyCallback(window, key_callback);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cout << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    /* init the triangle drawing */
    // define the vertex coordinates of the triangle
    float triangle[] = {
         0.5f,  0.5f, 1.0, 0.0, 0.0,
         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,

         0.5f, -0.5f, 1.0, 1.0, 0.0,
        -0.5f, -0.5f, 0.0, 0.0, 1.0,
        -0.5f,  0.5f, 0.0, 1.0, 0.0,
    };

    //Create initial transform matrices


    // create and bind the vertex buffer object and copy the data to the buffer
    GLuint VBO[1];
    glGenBuffers(1, VBO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(triangle), triangle, GL_STATIC_DRAW);

    // create and bind the vertex array object and describe data layout
    GLuint VAO[1];
    glGenVertexArrays(1, VAO);
    glBindVertexArray(VAO[0]);

    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5*sizeof(float), (void*)(0*sizeof(float)));
    glEnableVertexAttribArray(0);

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 5*sizeof(float), (void*)(2*sizeof(float)));
    glEnableVertexAttribArray(1);

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");


    Matrix transform = Matrix();
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {

        int transformLocation = glGetUniformLocation(shader.id(), "transformMat");
        // process input
        processInput(window);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // use the shader
        shader.use();

        /** Part 2 animate and scene by updating the transformation matrix */
        //Resets the transformation when changing animations
        if (spaceEvent){
            transform=Matrix();
            spaceEvent=false;
        }
        //Calls to update the transformation matrix
        transform = getTransform(transform);

        //Switch my Matrix class into an array
        float transformArray[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
        for (int row = 0; row<3; row++){
            for (int col = 0; col<3; col++){
                transformArray[row][col] = transform.values[row][col];
            }
        }
        //Add the transformation as a uniform
        glUniformMatrix3fv(transformLocation, 1, GL_FALSE, &transformArray[0][0]);

        // draw our triangles
        glBindVertexArray(VAO[0]);
        glDrawArrays(GL_TRIANGLES, 0, sizeof(triangle));

        /* Swap front and back * buffers */
        glfwSwapBuffers(window);

        /* Poll for and * process * events */
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}