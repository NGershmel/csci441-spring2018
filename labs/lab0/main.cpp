#include <stdio.h>
#include <iostream>
#include <string>

class Vector3 {
public:
    float x;
    float y;
    float z;

    // Constructor
    Vector3(float xx, float yy, float zz) : x(xx), y(yy), z(zz) {
        // nothing to do here as we've already initialized x, y, and z above
        std::cout << "in Vector3 constructor" << std::endl;
    }
	Vector3() : x(0), y(0), z(0) {
		std::cout << "generic Vector3 constructor" << std::endl;
	}

    // Destructor - called when an object goes out of scope or is destroyed
    ~Vector3() {
        // this is where you would release resources such as memory or file descriptors
        // in this case we don't need to do anything
        std::cout << "in Vector3 destructor" << std::endl;
    }
};

//Vector3 add(Vector3 v, Vector3 v2) { // v and v2 are copies, so any changes to them in this function
//    return Vector3(v.x + v2.x, v.y+v2.y, v.z+v2.z);
//}

//Vector3 operator+(Vector3 v, Vector3 v2) {
//	return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
//}

Vector3 add(const Vector3& v, const Vector3& v2) {
	return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
}

Vector3 operator+(const Vector3& v, const Vector3& v2) {
	return Vector3(v.x + v2.x, v.y + v2.y, v.z + v2.z);
}

std::ostream& operator<<(std::ostream& stream, const Vector3& v) {
	stream << v.x << "," << v.y << "," << v.z;
	return stream;
}

int main(int argc, char** argv) {;
    std::string myName;
    std::cin >> myName;
    std::cout << "Hello " << myName << std::endl;
    std::cout << "hello world" << argv[0] << " " << 1234 << " " << std::endl;
    Vector3 a(1,2,3);   // allocated to the stack
    Vector3 b(4,5,6);

    Vector3 c = add(a,b); // copies 6 floats to the arguments in add (3 per Vector3),
    std::cout << "The result is " << c.x << " " << c.y << " " << c.z << std::endl;
	Vector3 v(1, 2, 3);
	Vector3 v2(4, 5, 6);
	std::cout << v + v << std::endl;

	Vector3 myVector(0, 0, 0);
	myVector.y = 5;
	std::cout << myVector << std::endl;

	Vector3* heapVector = new Vector3(0, 0, 0);
	heapVector->y = 5;
	std::cout << *heapVector << std::endl;
	delete(heapVector);

	Vector3 array[10];
	Vector3* ptr = array;

	Vector3* vectorArray = new Vector3[10];
	for (int i = 0; i < 10; i++) {
		vectorArray[i].y = 5;
		std::cout << vectorArray[i] << std::endl;
	}
	delete[] vectorArray;
}