#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"

struct Viewport {
    glm::vec2 min;
    glm::vec2 max;

    Viewport(const glm::vec2& min, const glm::vec2& max)
        : min(min), max(max) {}
};

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct ray {
    glm::vec3 origin;
    glm::vec3 direction;
};


void render(bitmap_image& image, const std::vector<Sphere>& world) {
    rgb_t navy_blue = make_colour(20, 20, 80);

    //Render background
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            image.set_pixel(x, y, navy_blue);
        }
    }

    //Create ray array
    std::vector<std::vector<ray>> rays;
    rays.resize(image.height());
    for (int h = 0; h<image.height(); h++){
        rays[h].resize(image.width());
    }

    //Create camera info
    float ratio = ((float) image.width())/image.height();
    //std::cout << "Ratio: " << ratio << std::endl;
    float top = 5;
    float bot = -5;
    float left = -5*ratio;
    float right = 5*ratio;

    //Create rays
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            //create ray
            ray temp_ray;
            float tempx = left + ((right-left)*(x+0.5)) / image.width();
            float tempy = top + ((bot-top)*(y+0.5)) / image.height();
            temp_ray.origin = glm::vec3(tempx,tempy,0);
            temp_ray.direction = glm::vec3(0,0,-1);
            rays[y][x] = temp_ray;
            //std::cout << temp_ray.origin.x << ", " << temp_ray.origin.y << ", " << temp_ray.origin.z << std::endl;
        }
    }

    //Find intersection and color pixel per ray
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            //std::cout << "Testing ray " << x << ", " << y << std::endl;
            //std::cout << "\tOrigin: " << rays[y][x].origin.x << ", " << rays[y][x].origin.y << ", " << rays[y][x].origin.z << std::endl;
            float nearest_intersect = 100000;
            for(Sphere current_sphere : world) {
                glm::vec3 v_to_center = rays[y][x].origin - current_sphere.center;
                //std::cout << "Vector from origin to center of sphere: " << v_to_center.x << ", " << v_to_center.y << ", " << v_to_center.z << std::endl;
                //std::cout << "\tLength: " << glm::length(v_to_center) << std::endl;

                float test_for_intersect = (pow(glm::dot(rays[y][x].direction, v_to_center),2) - pow(glm::length(v_to_center),2) + pow(current_sphere.radius,2));
                //std::cout << test_for_intersect << std::endl;
                if (test_for_intersect >= 0 && glm::length(v_to_center)<nearest_intersect) {
                    nearest_intersect = glm::length(v_to_center);
                    rgb_t sphere_col = make_colour(current_sphere.color.x * 255, current_sphere.color.y * 255,
                                                       current_sphere.color.z * 255);
                    image.set_pixel(x, y, sphere_col);

                }
            }
        }
    }
}

void render_persp(bitmap_image& image, const std::vector<Sphere>& world) {
    rgb_t navy_blue = make_colour(20, 20, 80);

    //Render background
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            image.set_pixel(x, y, navy_blue);
        }
    }

    //Create ray array
    std::vector<std::vector<ray>> rays;
    rays.resize(image.height());
    for (int h = 0; h<image.height(); h++){
        rays[h].resize(image.width());
    }

    //Create camera info
    float ratio = ((float) image.width())/image.height();
    //std::cout << "Ratio: " << ratio << std::endl;
    float top = 2;
    float bot = -2;
    float left = -2*ratio;
    float right = 2*ratio;

    //Create rays
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            //create ray
            ray temp_ray;
            float d = 1;
            float tempx = left + ((right-left)*(x+0.5)) / image.width();
            float tempy = top + ((bot-top)*(y+0.5)) / image.height();
            glm::vec3 pixel_point(tempx,tempy,0);
            temp_ray.origin = glm::vec3(0,0,-d);
            temp_ray.direction = glm::normalize(temp_ray.origin - pixel_point);
            rays[y][x] = temp_ray;
            //std::cout << temp_ray.direction.x << ", " << temp_ray.direction.y << ", " << temp_ray.direction.z << std::endl;
        }
    }

    //Find intersection and color pixel per ray
    for (int y = 0; y < image.height(); y++) {
        for (int x = 0; x < image.width(); x++) {
            //std::cout << "Testing ray " << x << ", " << y << std::endl;
            //std::cout << "\tOrigin: " << rays[y][x].origin.x << ", " << rays[y][x].origin.y << ", " << rays[y][x].origin.z << std::endl;
            float nearest_intersect = 100000;
            for(Sphere current_sphere : world) {
                glm::vec3 v_to_center = rays[y][x].origin - current_sphere.center;
                //std::cout << "Vector from origin to center of sphere: " << v_to_center.x << ", " << v_to_center.y << ", " << v_to_center.z << std::endl;
                //std::cout << "\tLength: " << glm::length(v_to_center) << std::endl;

                float test_for_intersect = (pow(glm::dot(rays[y][x].direction, v_to_center),2) - pow(glm::length(v_to_center),2) + pow(current_sphere.radius,2));
                //std::cout << test_for_intersect << std::endl;
                if (test_for_intersect >= 0 && glm::length(v_to_center)<nearest_intersect) {
                    nearest_intersect = glm::length(v_to_center);
                    rgb_t sphere_col = make_colour(current_sphere.color.x * 255, current_sphere.color.y * 255,
                                                   current_sphere.color.z * 255);
                    image.set_pixel(x, y, sphere_col);

                }
            }
        }
    }
}

int main(int argc, char** argv) {

    // create an image 640 pixels wide by 480 pixels tall
    bitmap_image image_orth(640, 480);

    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
    };

    // render the world
    std::cout << "Orthographic ray tracing beginning..." << std::endl;
    render(image_orth, world);

    image_orth.save_image("ray-traced.bmp");

    bitmap_image image_persp(640, 480);
    std::cout << "Perspective ray tracing beginning..." << std::endl;
    render_persp(image_persp, world);
    image_persp.save_image("ray-traced-persp.bmp");
    std::cout << "Success" << std::endl;
}


