#ifndef _CSCI441_CAMERA_H_
#define _CSCI441_CAMERA_H_

#include <csci441/matrix.h>
#include <csci441/vector.h>
#include "Vector4.h"
#include "cmath"

class Camera {
public:
    Matrix projection;
    Matrix posRot;
    Matrix myPos;
    Matrix myRot;
    Vector4 eye;
    Vector4 origin;
    Vector4 up;
    bool isTop = false;

    Camera() : eye(0,0,0), origin(0,0,0), up(0,0,0) {}

    Matrix look_at() const {
        Matrix mat;
        mat.look_at(eye, origin, up);
        return mat;
    }

    Matrix pos2() const {
        if (!isTop) {
            Matrix retMat;
            float theta = atan(eye.x() / eye.z());
            retMat.values[0] = cos(theta);
            retMat.values[1] = 0;
            retMat.values[2] = -sin(theta);
            retMat.values[3] = 0;
            retMat.values[4] = 0;
            retMat.values[5] = 1;
            retMat.values[6] = 0;
            retMat.values[7] = 0;
            retMat.values[8] = sin(theta);
            retMat.values[9] = 0;
            retMat.values[10] = cos(theta);
            retMat.values[11] = 0;
            retMat.values[12] = origin.x();
            retMat.values[13] = origin.y();
            retMat.values[14] = origin.z();
            retMat.values[15] = 1;
            return retMat;
        } else {
            Matrix retMat;
            retMat.values[0] = 1;
            retMat.values[1] = 0;
            retMat.values[2] = 0;
            retMat.values[3] = 0;
            retMat.values[4] = 0;
            retMat.values[5] = cos(3.14159/2);
            retMat.values[6] = sin(3.14159/2);
            retMat.values[7] = 0;
            retMat.values[8] = 0;
            retMat.values[9] = -sin(3.14159/2);
            retMat.values[10] = cos(3.14159/2);
            retMat.values[11] = 0;
            retMat.values[12] = -6;
            retMat.values[13] = -10;
            retMat.values[14] = -4;
            retMat.values[15] = 1;
            return retMat;
        }
    }

    Matrix pos() const {
        if (!isTop) {
            return posRot;
        } else {
            Matrix retMat;
            retMat.values[0] = 1;
            retMat.values[1] = 0;
            retMat.values[2] = 0;
            retMat.values[3] = 0;
            retMat.values[4] = 0;
            retMat.values[5] = cos(3.14159/2);
            retMat.values[6] = sin(3.14159/2);
            retMat.values[7] = 0;
            retMat.values[8] = 0;
            retMat.values[9] = -sin(3.14159/2);
            retMat.values[10] = cos(3.14159/2);
            retMat.values[11] = 0;
            retMat.values[12] = -6;
            retMat.values[13] = -10;
            retMat.values[14] = -4;
            retMat.values[15] = 1;
            return retMat;
        }
    }

    Matrix getPosRot() const {
        return myPos * myRot;
    }
};

#endif
