//
// Created by garret on 4/1/18.
//

#ifndef GLFW_EXAMPLE_VECTOR4_H
#define GLFW_EXAMPLE_VECTOR4_H

#include <sstream>
#include <cmath>

class Vector4 {


public:
    float values[4];
    float coord(int idx) { return values[idx % 4]; }

    Vector4(float x, float y, float z, float w=1) : values{x, y, z, w} { }

    Vector4 operator-(const Vector4& v) const {
        return Vector4(
                values[0] - v.values[0],
                values[1] - v.values[1],
                values[2] - v.values[2],
                values[3] - v.values[3]);
    }

    float x() const { return values[0]; }
    void set_x (float new_val){
        values[0] = new_val;
    }
    float y() const { return values[1]; }
    void set_y (float new_val){
        values[1] = new_val;
    }
    float z() const { return values[2]; }
    void set_z (float new_val){
        values[2] = new_val;
    }
    float w() const { return values[3]; }
    void set_w (float new_val){
        values[3] = new_val;
    }

    float det(float a, float b, float c, float d) const {
        return  a*d - b*c;
    }

    Vector4 scale(double s) const {
        return Vector4(
                s*values[0],
                s*values[1],
                s*values[2]);
    }

    Vector4 normalized() const {
        double inv_norm = 1/norm();
        return scale(inv_norm);
    }

    Vector4 cross(const Vector4& v) const {
        return Vector4(
                det(values[1], values[2], v.values[1], v.values[2]),
                -det(values[0], values[2], v.values[0], v.values[2]),
                det(values[0], values[1], v.values[0], v.values[1]));
    }

    double norm() const {
        double n_sq = 0;
        for (int i = 0; i < 4; ++i) {
            n_sq += values[i]*values[i];
        }
        return sqrt(n_sq);
    }

    std::string to_string()  const {
        std::ostringstream os;
        for (float value : values) {
            os << value << " ";
        }
        return os.str();
    }

    friend std::ostream& operator<<(std::ostream& os, const Vector4& v) {
        os << v.to_string();
        return os;
    }
};

#endif //GLFW_EXAMPLE_VECTOR4_H
