//
// Created by garret on 3/31/18.
//

#ifndef GLFW_EXAMPLE_BADOBJLOADER_H
#define GLFW_EXAMPLE_BADOBJLOADER_H

#include "model.h"
#include "string.h"

class vnIndex {
public:
    unsigned int vertices[3];
    unsigned int normals[3];

    vnIndex(){

    }

    void addVertex(unsigned int vertex, int pos){
        vertices[pos] = vertex;
    }

    void addNormal(unsigned int normal, int pos){
        normals[pos] = normal;
    }
};

Model createModelFromObj(FILE* file, Shader shader){
    std::vector<float> vertices_array= std::vector<float>();
    std::vector<float> normal_array= std::vector<float>();
    std::vector<vnIndex> indices_array = std::vector<vnIndex>();

    if (file == NULL){
        std::cout << "File open error!" << std::endl;
        return Model(shader);
    }
    while(true) {
        char lineHeader[128];
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF){
            break;
        } else {
            float x,y,z;
            if (strcmp(lineHeader, "v") == 0) {
                fscanf(file, "%f %f %f\n", &x, &y, &z);
                vertices_array.push_back(x);
                vertices_array.push_back(y);
                vertices_array.push_back(z);
            } else if (strcmp(lineHeader, "vn") == 0) {
                fscanf(file, "%f %f %f\n", &x, &y, &z);
                normal_array.push_back(x);
                normal_array.push_back(y);
                normal_array.push_back(z);
            } else if (strcmp(lineHeader, "f") == 0){
                vnIndex myIndices = vnIndex();
                fscanf(file, "%u//%u %u//%u %u//%u\n", &myIndices.vertices[0], &myIndices.normals[0], &myIndices.vertices[1], &myIndices.normals[1], &myIndices.vertices[2], &myIndices.normals[2]);
                indices_array.push_back(myIndices);
            }
        }
    }
    std::vector<float> coords = std::vector<float>();
    for (vnIndex index : indices_array){
        coords.push_back(vertices_array[3*(index.vertices[0]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[0]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[0]-1)+2]);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(normal_array[3*(index.normals[0]-1)]);
        coords.push_back(normal_array[3*(index.normals[0]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[0]-1)+2]);

        coords.push_back(vertices_array[3*(index.vertices[1]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[1]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[1]-1)+2]);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(normal_array[3*(index.normals[1]-1)]);
        coords.push_back(normal_array[3*(index.normals[1]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[1]-1)+2]);

        coords.push_back(vertices_array[3*(index.vertices[2]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[2]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[2]-1)+2]);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(1);
        coords.push_back(normal_array[3*(index.normals[2]-1)]);
        coords.push_back(normal_array[3*(index.normals[2]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[2]-1)+2]);
    }
    Model objAsModel(coords, shader);

    return objAsModel;
}

Model createFromObjColor(FILE* file, Shader shader, float r, float g, float b){
    std::vector<float> vertices_array= std::vector<float>();
    std::vector<float> normal_array= std::vector<float>();
    std::vector<vnIndex> indices_array = std::vector<vnIndex>();

    if (file == NULL){
        std::cout << "File open error!" << std::endl;
        return Model(shader);
    }
    while(true) {
        char lineHeader[128];
        int res = fscanf(file, "%s", lineHeader);
        if (res == EOF){
            break;
        } else {
            float x,y,z;
            if (strcmp(lineHeader, "v") == 0) {
                fscanf(file, "%f %f %f\n", &x, &y, &z);
                vertices_array.push_back(x);
                vertices_array.push_back(y);
                vertices_array.push_back(z);
            } else if (strcmp(lineHeader, "vn") == 0) {
                fscanf(file, "%f %f %f\n", &x, &y, &z);
                normal_array.push_back(x);
                normal_array.push_back(y);
                normal_array.push_back(z);
            } else if (strcmp(lineHeader, "f") == 0){
                vnIndex myIndices = vnIndex();
                fscanf(file, "%u//%u %u//%u %u//%u\n", &myIndices.vertices[0], &myIndices.normals[0], &myIndices.vertices[1], &myIndices.normals[1], &myIndices.vertices[2], &myIndices.normals[2]);
                indices_array.push_back(myIndices);
            }
        }
    }
    std::vector<float> coords = std::vector<float>();
    for (vnIndex index : indices_array){
        coords.push_back(vertices_array[3*(index.vertices[0]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[0]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[0]-1)+2]);
        coords.push_back(r);
        coords.push_back(g);
        coords.push_back(b);
        coords.push_back(normal_array[3*(index.normals[0]-1)]);
        coords.push_back(normal_array[3*(index.normals[0]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[0]-1)+2]);

        coords.push_back(vertices_array[3*(index.vertices[1]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[1]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[1]-1)+2]);
        coords.push_back(r);
        coords.push_back(g);
        coords.push_back(b);
        coords.push_back(normal_array[3*(index.normals[1]-1)]);
        coords.push_back(normal_array[3*(index.normals[1]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[1]-1)+2]);

        coords.push_back(vertices_array[3*(index.vertices[2]-1)]);
        coords.push_back(vertices_array[3*(index.vertices[2]-1)+1]);
        coords.push_back(vertices_array[3*(index.vertices[2]-1)+2]);
        coords.push_back(r);
        coords.push_back(g);
        coords.push_back(b);
        coords.push_back(normal_array[3*(index.normals[2]-1)]);
        coords.push_back(normal_array[3*(index.normals[2]-1)+1]);
        coords.push_back(normal_array[3*(index.normals[2]-1)+2]);
    }
    Model objAsModel(coords, shader);

    return objAsModel;
}

#endif //GLFW_EXAMPLE_BADOBJLOADER_H
