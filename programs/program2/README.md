# Maze Game - Noah Gershmel

The goal is to reach the small white square found at the right side of the maze

## Controls

* W - Move forward
* S - Move backwards
* A - Strafe left
* D - Strafe right
* Q - Turn left
* E - Turn right
* SPACE - Holding space allows you to view the map from above. I chose holding to encourage the player to avoid it.

Character and map are self modeled and loaded with my added badObjLoader.h file.