#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include <csci441/matrix.h>
#include <csci441/matrix3.h>
#include <csci441/vector.h>
#include <csci441/uniform.h>

#include "shape.h"
#include "model.h"
#include "camera.h"
#include "renderer.h"
#include "badObjLoader.h"

const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
bool topView = false;
float theta;

void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

bool isReleased(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_RELEASE;
}

Matrix processModel(const Matrix& model, const Camera& cam, GLFWwindow *window) {
    Matrix trans;
    const float TRANS = .01;
    float theta_rad = theta * (0.01745329252f);
    float theta_right = (theta + 90) * (0.01745329252f);

    if (isPressed(window, GLFW_KEY_W)) {
        trans.translate(0.2f * sin(theta_rad), 0, -0.2f * cos(theta_rad));
    } else if (isPressed(window, GLFW_KEY_S)) {
        trans.translate(-0.2f * sin(theta_rad), 0, 0.2f * cos(theta_rad));
    } else if (isPressed(window, GLFW_KEY_A)) {
        trans.translate(-0.2f * sin(theta_right), 0, 0.2f * cos(theta_right));
    } else if (isPressed(window, GLFW_KEY_D)) {
        trans.translate(0.2f * sin(theta_right), 0, -0.2f * cos(theta_right));
    }
    //trans.values[12] = -cam.myPos.values[12];
    //trans.values[13] = -cam.myPos.values[13] - 1.0f;
    //trans.values[14] = -cam.myPos.values[14];

    return trans * model;
}

Camera processCam(Camera& camera, GLFWwindow *window){
    Matrix change;
    if (isPressed(window, GLFW_KEY_W)) {
        camera.origin.set_z(camera.origin.values[2] + 0.2f);
        change.translate(0,0,0.2);
        camera.myPos = change * camera.myPos;
    } else if (isPressed(window, GLFW_KEY_S)) {
        camera.origin.set_z(camera.origin.values[2] - 0.2f);
        change.translate(0,0,-0.2f);
        camera.myPos = change * camera.myPos;
    } else if (isPressed(window, GLFW_KEY_A)) {
        camera.origin.set_x(camera.origin.values[0] + 0.2f);
        change.translate(0.2,0,0);
        camera.myPos = change * camera.myPos;
    } else if (isPressed(window, GLFW_KEY_D)) {
        camera.origin.set_x(camera.origin.values[0] - 0.2f);
        change.translate(-0.2f,0,0);
        camera.myPos = change * camera.myPos;
    } else if (isPressed(window, GLFW_KEY_Q)) {
        theta-=10;
        change.rotate_y(-10);
        camera.myRot = change * camera.myRot;
    } else if (isPressed(window, GLFW_KEY_E)) {
        theta+=10;
        change.rotate_y(10);
        camera.myRot = change * camera.myRot;
    }
    camera.posRot = change * camera.posRot;
    return camera;
}

void processInput(Matrix& model, Camera& camera, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE)) {
        glfwSetWindowShouldClose(window, true);
    } else topView = isPressed(window, GLFW_KEY_SPACE);
    model = processModel(model, camera, window);
}

void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

int main(void) {
    GLFWwindow* window;

    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "CSCI441-lab", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    // create obj
    //Model pers(
    //        DiscoCube().coords,
    //        Shader("../vert.glsl", "../frag.glsl"));

    FILE* player_file = fopen("../car.obj", "r");
    Model pers = createFromObjColor(player_file, Shader("../vert.glsl", "../frag.glsl"), 1.0, 1.0, 0.0);

    // make a floor
    Model floor(
            DiscoCube().coords,
            Shader("../vert.glsl", "../frag.glsl"));

    //Make the maze
    FILE* maze_file = fopen("../maze2.obj", "r");
    Model maze = createModelFromObj(maze_file, Shader("../vert.glsl", "../frag.glsl"));

    Matrix pers_trans, pers_scale, pers_rotate;
    pers_trans.translate(-1.1f, -0.5f, 2.7f);
    pers_scale.scale(1,1,1);
    pers_rotate.rotate_x(90);
    pers.model = pers_trans * pers_scale * pers_rotate;

    Matrix maze_trans, maze_scale;
    maze_trans.translate(-3, 0, 0);
    maze_scale.scale(1, 1, 1);
    maze.model = maze_trans*maze_scale;

    Matrix floor_trans, floor_scale;
    floor_trans.translate(0, -2, 0);
    floor_scale.scale(100, 1, 100);
    floor.model = floor_trans*floor_scale;

    // setup camera
    Matrix projection;
    projection.perspective(45, 1, .01, 20);

    Matrix orthographic;
    orthographic.ortho(-20, 20, -20, 20, .01, 50);

    Camera camera;
    camera.posRot = Matrix();
    camera.posRot.translate(1.1, 0, -2.7f);
    camera.myPos.translate(1.1, 0, -2.7f);
    camera.projection = projection;
    camera.eye = Vector4(0, 0, 3);
    camera.origin = Vector4(1.1, 0, -2.7f);
    camera.up = Vector4(0, 1, 0);

    Camera topCam;
    topCam.isTop = true;
    topCam.projection = orthographic;
    topCam.eye = Vector4(0,0,3);
    topCam.origin = Vector4(1.1,0,-2.7f);
    topCam.up = Vector4(0,0,-1);

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);

    // create a renderer
    Renderer renderer;

    // set the light position
    Vector lightPos(3.75f, 3.75f, 4.0f);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(pers.model, camera, window);
        camera = processCam(camera, window);

        //I rate my workaround a solid 8/10
        Matrix rot = pers.model;
        Matrix id;
        rot.rotate_z(theta);

        /* Render here */
        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // render the object and the floor
        if (!topView) {
            renderer.render(camera, pers, rot, lightPos);
            renderer.render(camera, floor, id, lightPos);
            renderer.render(camera, maze, id,lightPos);
        } else {
            renderer.render(topCam, pers, rot,lightPos);
            renderer.render(topCam, floor, id,lightPos);
            renderer.render(topCam, maze, id,lightPos);
        }

        /* Swap front and back and poll for io events */
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}
