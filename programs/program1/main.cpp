#include <iostream>
#include <sstream>
#include <fstream>
#include <cmath>

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <csci441/shader.h>
#include "Matrix.cpp"
#include "shape.h"

//Screen constants
const int SCREEN_WIDTH = 1280;
const int SCREEN_HEIGHT = 960;
//Screen min and maxes
float min_x = -1;
float max_x = 1;
float min_y = -1;
float max_y = 1;
float start_max_x, start_max_y,  start_min_x,  start_min_y;

//Vertex buffer object pointer
GLuint VBO1;

//Shape to act as a cursor in edit mode
Shape pointerShape = Shape();

//Arrays to hold the global objects and groups
std::vector<Shape> objects;
std::vector<group> groups;

//Position of the selected object or group within their respective arrays
int selObject = -1;
int selGroup = -1;

//Shapes that will be added to the next group
std::vector<int> toGroup;

//Vertices currently in the queue
std::vector<float> vertexQueue;

//Mode variables
int mode = 0;
int subMode = 0;
int step = 0;
int primitiveSelected = 0;

//Mouse control variables
bool drag = false;
double start_x, start_y;

//Primitive control variables
int circleRes = 5;
Shape prim;

//Clamps a value to the min and max range
float clamp(float val, float min, float max){
    if (val>max){
        return max;
    } else if (val<min) {
        return min;
    } else {
        return val;
    }
}

//Callback function for the frame buffer
void framebufferSizeCallback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

//Determines whether or not a key is pressed
bool isPressed(GLFWwindow *window, int key) {
    return glfwGetKey(window, key) == GLFW_PRESS;
}

//Builds a model matrix that gets passed in
Matrix3 processModel(const Matrix3& model, GLFWwindow *window) {
    Matrix3 trans;

    const float ROT = .01;
    const float SCALE = .05;
    const float TRANS = .01;

    // ROTATE
    if (isPressed(window, '[')) { trans = Matrix3::rotateCCW (ROT); }
    else if (isPressed(window, ']')) { trans = Matrix3::rotateCW(ROT); }
        // SCALE
    else if (isPressed(window, ',')) { trans = Matrix3::scale(1-SCALE, 1-SCALE); }
    else if (isPressed(window, '.')) { trans = Matrix3::scale(1+SCALE, 1+SCALE); }
        // TRANSLATE
    else if (isPressed(window, GLFW_KEY_UP)) { trans = Matrix3::translate(0, TRANS); }
    else if (isPressed(window, GLFW_KEY_DOWN)) { trans = Matrix3::translate(0, -TRANS); }
    else if (isPressed(window, GLFW_KEY_LEFT)) { trans = Matrix3::translate(-TRANS, 0); }
    else if (isPressed(window, GLFW_KEY_RIGHT)) { trans = Matrix3::translate(TRANS, 0); }
    return trans * model;
}

//Processes the view by determining the new camera position
Matrix3 processView(Matrix3 camera, GLFWwindow *window){
    Matrix3 change = Matrix3();
    float values[3][3] = {
            {2/(max_x-min_x), 0, -((max_x+min_x)/(max_x-min_x))},
            {0, 2/(max_y-min_y), -((max_y+min_y)/(max_y-min_y))},
            {0,0,1},
    };
    change.setValues(values);
    return change;
}

//Controls the inputs for most of the program
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods){
    if (glfwGetTime()>15) { //Ensures the program has started
        const float TRANS = .01; //Constant for translations

        //Scaling the viewport
        if (key == GLFW_KEY_EQUAL && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            {
                min_x = min_x * (0.99f);
                max_x = max_x * (0.99f);
                min_y = min_y * (0.99f);
                max_y = max_y * (0.99f);
            }
        } else if (key == GLFW_KEY_MINUS && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            {
                min_x = min_x * (1.01f);
                max_x = max_x * (1.01f);
                min_y = min_y * (1.01f);
                max_y = max_y * (1.01f);
            }
        }
        //Moving the viewport
        if (key == GLFW_KEY_W && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            min_y += TRANS;
            max_y += TRANS;
        } else if (key == GLFW_KEY_S && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            min_y -= TRANS;
            max_y -= TRANS;
        } else if (key == GLFW_KEY_A && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            min_x -= TRANS;
            max_x -= TRANS;
        } else if (key == GLFW_KEY_D && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            min_x += TRANS;
            max_x += TRANS;
        }
        //Rotations and scales for groups and shapes
        else if (key == GLFW_KEY_LEFT_BRACKET && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                groups[selGroup].rotation = groups[selGroup].rotation * Matrix3::rotateCCW(.05f);
            } else if (mode == 6) {
                objects[selObject].rotation = objects[selObject].rotation * Matrix3::rotateCCW(.05f);
            }
        } else if (key == GLFW_KEY_RIGHT_BRACKET && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                groups[selGroup].rotation = groups[selGroup].rotation * Matrix3::rotateCW(0.05f);
            } else if (mode == 6) {
                objects[selObject].rotation = objects[selObject].rotation * Matrix3::rotateCW(0.05f);
            }
        } else if (key == GLFW_KEY_O && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                groups[selGroup].scale = groups[selGroup].scale * Matrix3::scale(0.99, 0.99);
            } else if (mode == 6) {
                objects[selObject].scale = objects[selObject].scale * Matrix3::scale(0.99f, 0.99f);
            }
        } else if (key == GLFW_KEY_P && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                groups[selGroup].scale = groups[selGroup].scale * Matrix3::scale(1.01f, 1.01f);
            } else if (mode == 6) {
                objects[selObject].scale = objects[selObject].scale * Matrix3::scale(1.01f, 1.01f);
            }
        }
        //Color controls
        else if (key == GLFW_KEY_R && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[0] = clamp(object.myColor[0] + 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[0] = clamp(objects[selObject].myColor[0] + 0.05f, 0, 1);
            }
        } else if (key == GLFW_KEY_E && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[0] = clamp(object.myColor[0] - 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[0] = clamp(objects[selObject].myColor[0] - 0.05f, 0, 1);
            }
        } else if (key == GLFW_KEY_G && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[1] = clamp(object.myColor[1] + 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[1] = clamp(objects[selObject].myColor[1] + 0.05f, 0, 1);
            }
        } else if (key == GLFW_KEY_F && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[1] = clamp(object.myColor[1] - 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[1] = clamp(objects[selObject].myColor[1] - 0.05f, 0, 1);
            }
        } else if (key == GLFW_KEY_B && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[2] = clamp(object.myColor[2] + 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[2] = clamp(objects[selObject].myColor[2] + 0.05f, 0, 1);
            }
        } else if (key == GLFW_KEY_V && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 5) {
                for (auto &object : objects) {
                    if (object.myGroup == objects[selObject].myGroup) {
                        object.myColor[2] = clamp(object.myColor[2] - 0.05f, 0, 1);
                    }
                }
            }
            if (mode == 6) {
                objects[selObject].myColor[2] = clamp(objects[selObject].myColor[2] - 0.05f, 0, 1);
            }
        }
        //Arrow controls
        else if (key == GLFW_KEY_UP && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 0) {

            } else if (mode == 1) {
                pointerShape.position = pointerShape.position * Matrix3::translate(0, 0.01);
            } else if (mode == 2 && subMode == 0 && step == 0) {
                circleRes = (int) clamp(circleRes + 1, 3, 16);
            } else if (mode == 2 && subMode == 0 && step == 1) {
                prim.position = prim.position * Matrix3::translate(0, 0.05f);
            } else if (mode == 2 && subMode == 0 && step == 2) {
                prim.scale = prim.scale * Matrix3::scale(1, 1.1);
            } else if (mode == 5) {
                groups[selGroup].position = groups[selGroup].position * Matrix3::translate(0, 0.01);
            } else if (mode == 6) {
                objects[selObject].position = objects[selObject].position * Matrix3::translate(0, 0.01f);
            }
        } else if (key == GLFW_KEY_DOWN && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 0) {

            } else if (mode == 1) {
                pointerShape.position = pointerShape.position * Matrix3::translate(0, -0.01f);
            } else if (mode == 2 && subMode == 0 && step == 0) {
                circleRes = (int) clamp(circleRes - 1, 3, 16);
            } else if (mode == 2 && subMode == 0 && step == 1) {
                prim.position = prim.position * Matrix3::translate(0, -0.05f);
            } else if (mode == 2 && subMode == 0 && step == 2) {
                prim.scale = prim.scale * Matrix3::scale(1, 0.9f);
            } else if (mode == 5) {
                groups[selGroup].position = groups[selGroup].position * Matrix3::translate(0, -0.01f);
            } else if (mode == 6) {
                objects[selObject].position = objects[selObject].position * Matrix3::translate(0, -0.01f);
            }
        } else if (key == GLFW_KEY_RIGHT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 0) {
                int last = selObject;
                selObject += 1;
                if (last != -1) {
                    objects[last].active = false;
                }
                if (selObject > objects.size() - 1) {
                    selObject = 0;
                    objects[last].selected = false;
                } else {
                    objects[selObject].selected = true;
                    objects[selObject].active = true;
                    if (objects[selObject].myGroup != -1) {
                        for (auto &object : objects) {
                            object.selected = object.myGroup == objects[selObject].myGroup;
                        }
                    } else {
                        for (auto &object : objects) {
                            if (objects[selObject].id != object.id) {
                                object.selected = false;
                            }
                        }
                    }
                }
            } else if (mode == 1) {
                pointerShape.position = pointerShape.position * Matrix3::translate(0.01, 0);
            } else if (mode == 2) {
                if (subMode == 0 && step == 0) {
                    primitiveSelected++;
                    if (primitiveSelected > 2) {
                        primitiveSelected = 0;
                    }
                } else if (subMode == 0 && step == 1) {
                    prim.position = prim.position * Matrix3::translate(0.05f, 0);
                } else if (subMode == 0 && step == 2) {
                    prim.scale = prim.scale * Matrix3::scale(1.1f, 1);
                } else if (subMode == 0 && step == 3) {
                    prim.rotation = prim.rotation * Matrix3::rotateCW(.05f);
                }
            } else if (mode == 5) {
                groups[selGroup].position = groups[selGroup].position * Matrix3::translate(0.01f, 0);
            } else if (mode == 6) {
                objects[selObject].position = objects[selObject].position * Matrix3::translate(0.01f, 0);
            }
        } else if (key == GLFW_KEY_LEFT && (action == GLFW_PRESS || action == GLFW_REPEAT)) {
            if (mode == 0) {
                int last = selObject;
                selObject -= 1;
                if (last != -1) {
                    objects[last].active = false;
                }
                if (selObject < 1) {
                    selObject = 0;
                    objects[last].selected = false;
                } else {
                    objects[selObject].selected = true;
                    objects[selObject].active = true;
                    if (objects[selObject].myGroup != -1) {
                        for (auto &object : objects) {
                            object.selected = object.myGroup == objects[selObject].myGroup;
                        }
                    } else {
                        for (auto &object : objects) {
                            if (objects[selObject].id != object.id) {
                                object.selected = false;
                            }
                        }
                    }
                }

            } else if (mode == 1) {
                pointerShape.position = pointerShape.position * Matrix3::translate(-0.01f, 0);
            } else if (mode == 2) {
                if (subMode == 0 && step == 0) {
                    primitiveSelected--;
                    if (primitiveSelected < 0) {
                        primitiveSelected = 2; //Max primitive
                    }
                } else if (subMode == 0 && step == 1) {
                    prim.position = prim.position * Matrix3::translate(-0.05f, 0);
                } else if (subMode == 0 && step == 2) {
                    prim.scale = prim.scale * Matrix3::scale(0.9f, 1);
                } else if (subMode == 0 && step == 3) {
                    prim.rotation = prim.rotation * Matrix3::rotateCCW(.05f);
                }
            } else if (mode == 5) {
                groups[selGroup].position = groups[selGroup].position * Matrix3::translate(-0.01f, 0);
            } else if (mode == 6) {
                objects[selObject].position = objects[selObject].position * Matrix3::translate(-0.01f, 0);
            }
        }

        //Controls for switching modes
        if (key == GLFW_KEY_TAB && action == GLFW_PRESS) {
            if (mode == 1) {
                mode = 0;
                step = 0;
                objects.push_back(prim);
                objects[objects.size() - 1].id = objects.size() - 1;
            } else if (mode == 0) {
                prim = Shape();
                pointerShape.position = Matrix3::translate((max_x + min_x) / 2, (max_y + min_y) / 2);
                prim.coords = std::vector<float>();
                step = 0;
                mode = 1;
            } else if (mode == 2) {
                mode = 0;
            }
        } else if (key == GLFW_KEY_N && action == GLFW_PRESS) {
            mode = 2;
            subMode = 0;
            step = 0;
            prim.position = Matrix3::translate((max_x + min_x) / 2, (max_y + min_y) / 2);
        } else if (key == GLFW_KEY_M && action == GLFW_PRESS) {
            if (objects[selObject].myGroup != -1) {
                if (mode == 0) {
                    mode = 5;
                    selGroup = objects[selObject].myGroup;
                } else if (mode == 5) {
                    mode = 0;
                    selGroup = -1;
                }
            } else {
                if (mode == 0) {
                    mode = 6;
                } else if (mode == 6) {
                    mode = 0;
                }
            }
        }

        //Moving through steps
        if (key == GLFW_KEY_ENTER && action == GLFW_PRESS) {
            if (mode == 0) {
                for (int i = 0; i < objects.size(); i++) {
                    if (objects[i].selected) {
                        toGroup.push_back(i);
                        objects[i].toGroup = true;
                    }
                }
            }
            if (mode == 1) {
                float temp_x = pointerShape.position.values[0][2];
                float temp_y = pointerShape.position.values[1][2];
                if (step == 0) {
                    vertexQueue = std::vector<float>();
                    add_vertex(prim.coords, temp_x, temp_y);
                } else if (step == 1 || step == 2) {
                    vertexQueue.push_back(temp_x);
                    vertexQueue.push_back(temp_y);
                    add_vertex(prim.coords, temp_x, temp_y);
                } else {
                    float x_1 = vertexQueue[0];
                    float y_1 = vertexQueue[1];
                    float x_2 = vertexQueue[2];
                    float y_2 = vertexQueue[3];
                    vertexQueue = std::vector<float>();
                    add_vertex(prim.coords, x_1, y_1);
                    add_vertex(prim.coords, x_2, y_2);
                    add_vertex(prim.coords, temp_x, temp_y);
                    vertexQueue.push_back(x_2);
                    vertexQueue.push_back(y_2);
                    vertexQueue.push_back(temp_x);
                    vertexQueue.push_back(temp_y);
                }
                step++;
            }
            if (mode == 2) {
                step++;
            }
            if (mode == 2 && step == 4) {
                objects.push_back(prim);
                objects[objects.size() - 1].id = objects.size() - 1;
                prim = Shape();
                mode = 0;
            }
        }


        //Grouping controls
        if (key == GLFW_KEY_G && action == GLFW_PRESS && mode == 0) {
            group addGroup = group();
            for (auto item : toGroup) {
                objects[item].myGroup = groups.size();
                objects[item].toGroup = false;
                addGroup.shapes.push_back(objects[item]);
            }
            groups.push_back(addGroup);
            toGroup = std::vector<int>();

        } else if (key == GLFW_KEY_U && action == GLFW_PRESS && mode == 0) {
            objects[selObject].myGroup = -1;
        }

        //Triangulation reset
        if (key == GLFW_KEY_T && action == GLFW_PRESS && mode == 1) {
            step = 0;
        }
    }
}

//Control function for when the mouse is being dragged
void cursor_position_callback(GLFWwindow* window, double current_x, double current_y){
    if (drag) {
        float delta_x = (float) (current_x - start_x);
        float delta_y = (float) (current_y - start_y);
        max_x = start_max_x - (delta_x/(SCREEN_WIDTH));
        min_x = start_min_x - (delta_x/(SCREEN_WIDTH));
        max_y = start_max_y + (delta_y/(SCREEN_HEIGHT));
        min_y = start_min_y + (delta_y/(SCREEN_HEIGHT));
    }
}

//Controls when a drag is begun and sets up the necessary information
void mouse_button_callback(GLFWwindow* window, int button, int action, int mods){
    if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS) {
        start_max_x = max_x;
        start_min_x = min_x;
        start_max_y = max_y;
        start_min_y = min_y;
        glfwGetCursorPos(window, &start_x, &start_y);
        drag = true;
    } else if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_RELEASE) {
        drag = false;
    }
}

//Processes the user's input every frame
void processInput(Matrix3& model, Matrix3& camera, GLFWwindow *window) {
    if (isPressed(window, GLFW_KEY_ESCAPE) || isPressed(window, GLFW_KEY_Q)) {
        glfwSetWindowShouldClose(window, true);
    }
    model = processModel(model, window);
    camera = processView(camera, window);
}


//Handles the error calls
void errorCallback(int error, const char* description) {
    fprintf(stderr, "GLFW Error: %s\n", description);
}

//Creates all of the vertex buffer objects, 1 per shape
void copyDataDiscreteVBO(){
    for (auto &object : objects) {
        glGenBuffers(1, &object.VBO);
        glBindBuffer(GL_ARRAY_BUFFER, object.VBO);
        glBufferData(GL_ARRAY_BUFFER, object.coords.size()*sizeof(float), &object.coords[0], GL_STATIC_DRAW);
        glGenVertexArrays(1, &object.VAO);
        glBindVertexArray(object.VAO);
        glBindBuffer(GL_ARRAY_BUFFER, object.VAO);
        glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE, 2*sizeof(float), (void*)(0));
        glEnableVertexAttribArray(0);
    }
}

//Render associated with copyDataDiscreteVBO
void renderByDiscreteVBO(Shader &shader, Matrix3 cameraInput) {
    float overrideColor[] = {0.8f,0.8f,0};
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int colorObjLoc = glGetUniformLocation(shader.id(), "objColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    for (auto &object : objects) {
        if (object.toGroup){
            overrideColor[0] = 1.0;
            overrideColor[1] = 0;
            overrideColor[2] = 0;
        } else if (object.active) {
            overrideColor[0] = 0;
            overrideColor[1] = 0.8f;
            overrideColor[2] = 0.2f;
        }
        else {
            overrideColor[0] = 0.8;
            overrideColor[1] = 0.8;
            overrideColor[2] = 0;
        }

        if (object.myGroup == -1) {
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &object.getModelMatrix().values[0][0]);
        } else {
            Matrix3 newPos = object.position * groups[object.myGroup].position;
            Matrix3 newRot = object.rotation * groups[object.myGroup].rotation;
            Matrix3 newSca = object.scale * groups[object.myGroup].scale;
            Matrix3 newMod = newPos * newRot * newSca;
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &newMod.values[0][0]);
        }
        glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
        if (object.selected || object.toGroup || object.active){
            glUniform3fv(colorOverLoc, 1, &overrideColor[0]);
        } else {
            glUniform3fv(colorOverLoc, 1, &object.myColor[0]);
            glUniform3fv(colorObjLoc, 1, &object.myColor[0]);
        }
        glBindVertexArray(object.VAO);
        //glDrawArrays(GL_LINE_LOOP, 0, object.coords.size() * sizeof(float));
        glDrawArrays(GL_TRIANGLES, 0, object.coords.size() * sizeof(float));
    }
}

//Copies all object data to a single vertex buffer object
void copyData(){
    int totalVertices = 0;
    for (auto &object : objects) {
        //object.printCoords();
        totalVertices += object.coords.size();
    }
    //bind vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, totalVertices*sizeof(float), nullptr, GL_STATIC_DRAW);

    totalVertices = 0;
    for (auto object : objects) {
        glBufferSubData(GL_ARRAY_BUFFER, totalVertices * sizeof(float), object.coords.size()*sizeof(float),
                        &object.coords[0]);
        totalVertices+=object.coords.size();
    }

    totalVertices=0;
    for (auto &object : objects) {
        glGenVertexArrays(1, &object.VAO);
        glBindVertexArray(object.VAO);
        glBindBuffer(GL_ARRAY_BUFFER, VBO1);
        glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE, 2*sizeof(float), (void*) (totalVertices*sizeof(float)));
        glEnableVertexAttribArray(0);
        totalVertices+=object.coords.size();
    }
}

//Render associated with copyData
void renderMyObjects(Shader &shader, Matrix3 cameraInput){
    float overrideColor[] = {0.8f,0.8f,0};
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int colorObjLoc = glGetUniformLocation(shader.id(), "objColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    for (auto &object : objects) {
        if (object.toGroup){
            overrideColor[0] = 1.0;
            overrideColor[1] = 0;
            overrideColor[2] = 0;
        } else if (object.active) {
            overrideColor[0] = 0;
            overrideColor[1] = 0.8f;
            overrideColor[2] = 0.2f;
        }
        else {
            overrideColor[0] = 0.8;
            overrideColor[1] = 0.8;
            overrideColor[2] = 0;
        }

        if (object.myGroup == -1) {
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &object.getModelMatrix().values[0][0]);
        } else {
            Matrix3 newPos = object.position * groups[object.myGroup].position;
            Matrix3 newRot = object.rotation * groups[object.myGroup].rotation;
            Matrix3 newSca = object.scale * groups[object.myGroup].scale;
            Matrix3 newMod = newPos * newRot * newSca;
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &newMod.values[0][0]);
        }
        glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
        if (object.selected || object.toGroup || object.active){
            glUniform3fv(colorOverLoc, 1, &overrideColor[0]);
        } else {
            glUniform3fv(colorOverLoc, 1, &object.myColor[0]);
            glUniform3fv(colorObjLoc, 1, &object.myColor[0]);
        }
        glBindVertexArray(object.VAO);
        //glDrawArrays(GL_LINE_LOOP, 0, object.coords.size() * sizeof(float));
        glDrawArrays(GL_TRIANGLES, 0, object.coords.size() * sizeof(float));
    }
}

//Renders the objects with their colors regardless of selection
void renderActualColors(Shader &shader, Matrix3 cameraInput){
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int colorObjLoc = glGetUniformLocation(shader.id(), "objColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    for (auto &object : objects) {
        if (object.myGroup == -1) {
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &object.getModelMatrix().values[0][0]);
        } else {
            Matrix3 newPos = object.position * groups[object.myGroup].position;
            Matrix3 newRot = object.rotation * groups[object.myGroup].rotation;
            Matrix3 newSca = object.scale * groups[object.myGroup].scale;
            Matrix3 newMod = newPos * newRot * newSca;
            glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &newMod.values[0][0]);
        }
        glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
        glUniform3fv(colorOverLoc, 1, &object.myColor[0]);
        glUniform3fv(colorObjLoc, 1, &object.myColor[0]);
        glBindVertexArray(object.VAO);
        glDrawArrays(GL_TRIANGLES, 0, object.coords.size() * sizeof(float));
    }
}

//Copies over the selected objects only
void copySelectedOnly(){
    //bind vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, objects[selObject].coords.size()*sizeof(float), &objects[selObject].coords[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &objects[selObject].VAO);
    glBindVertexArray(objects[selObject].VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE, 2*sizeof(float), (void*) (0*sizeof(float)));
    glEnableVertexAttribArray(0);
}

//Renders only the selected object to the back buffer
void renderSelectedOnly(Shader &shader, Matrix3 cameraInput){
    float stdColor[] = {0,0,0};
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &objects[selObject].getModelMatrix().values[0][0]);
    glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
    glUniform3fv(colorOverLoc, 1, &stdColor[0]);

    glBindVertexArray(objects[selObject].VAO);
    glDrawArrays(GL_TRIANGLES, 0, pointerShape.coords.size() * sizeof(float));

}

//Copies the stamp primitive
void copyStamp(){
    if (mode == 2) {
        if (primitiveSelected == 0) {
            prim.generateTriangle();
        } else if (primitiveSelected == 1) {
            prim.generateSquare();
        } else if (primitiveSelected == 2) {
            prim.generateCircle(circleRes);
        }
    }
    //bind vertices
    glGenBuffers(1, &prim.VBO);
    glBindBuffer(GL_ARRAY_BUFFER, prim.VBO);
    glBufferData(GL_ARRAY_BUFFER, prim.coords.size()*sizeof(float), &prim.coords[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &prim.VAO);
    glBindVertexArray(prim.VAO);
    glBindBuffer(GL_ARRAY_BUFFER, prim.VBO);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE, 2*sizeof(float), (void*) (0*sizeof(float)));
    glEnableVertexAttribArray(0);
}

//Renders the stamp to the back buffer
void renderStamp(Shader &shader, Matrix3 cameraInput){
    float stdColor[] = {0,0,0};
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &prim.getModelMatrix().values[0][0]);
    glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
    glUniform3fv(colorOverLoc, 1, &stdColor[0]);

    glBindVertexArray(prim.VAO);
    glDrawArrays(GL_TRIANGLES, 0, pointerShape.coords.size() * sizeof(float));

}

//Copies the pointer object over to the buffer
void copyPointer(){
    //bind vertices
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glBufferData(GL_ARRAY_BUFFER, pointerShape.coords.size()*sizeof(float), &pointerShape.coords[0], GL_STATIC_DRAW);

    glGenVertexArrays(1, &pointerShape.VAO);
    glBindVertexArray(pointerShape.VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO1);
    glVertexAttribPointer(0,2,GL_FLOAT,GL_FALSE, 2*sizeof(float), (void*) (0*sizeof(float)));
    glEnableVertexAttribArray(0);
}

//Renders the pointer to the back buffer
void renderPointer(Shader &shader, Matrix3 cameraInput){
    float stdColor[] = {0,0,0};
    int colorOverLoc = glGetUniformLocation(shader.id(), "selColor");
    int modelLoc = glGetUniformLocation(shader.id(), "model");
    int cameraLoc = glGetUniformLocation(shader.id(), "camera");

    Matrix3 positionOnly = Matrix3();
    float values[3][3] = {
            {1,0,cameraInput.values[0][2]},
            {0,1,cameraInput.values[1][2]},
            {0,0,1}
    };
    positionOnly.setValues(values);

    glUniformMatrix3fv(modelLoc, 1, GL_FALSE, &pointerShape.getModelMatrix().values[0][0]);
    glUniformMatrix3fv(cameraLoc, 1, GL_FALSE, &cameraInput.values[0][0]);
    glUniform3fv(colorOverLoc, 1, &stdColor[0]);

    glBindVertexArray(pointerShape.VAO);
    glDrawArrays(GL_TRIANGLES, 0, pointerShape.coords.size() * sizeof(float));

}

int main(void) {
    //Create the window
    GLFWwindow* window;

    //Prep for errors
    glfwSetErrorCallback(errorCallback);

    /* Initialize the library */
    if (!glfwInit()) { return -1; }

    //Necessary to avoid the flames of hell
    glfwWindowHint (GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint (GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint (GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Noah Gershmel\'s 2D Modeler", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    /* Make the window's context current */
    glfwMakeContextCurrent(window);

    //Prepare callbacks for keys and mice
    glfwSetKeyCallback(window, key_callback);
    glfwSetMouseButtonCallback(window, mouse_button_callback);
    glfwSetCursorPosCallback(window, cursor_position_callback);

    // tell glfw what to do on resize
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);

    // init glad
    if (!gladLoadGL()) {
        std::cerr << "Failed to initialize OpenGL context" << std::endl;
        glfwTerminate();
        return -1;
    }

    //Create the cursor
    pointerShape.generatePointer();

    // Test shapes. Initializes the object array
    Shape c = Shape();
    c.generateTriangle();
    c.id = 0;
    c.position = Matrix3::translate(1000,1000);
    objects.push_back(c);

    //Camera control matrix
    Matrix3 camera;

    // create the shaders
    Shader shader("../vert.glsl", "../frag.glsl");
    Shader pointer("../pointerVert.glsl", "../frag.glsl");

    // setup the textures
    shader.use();
    pointer.use();

    // set the matrices
    Matrix3 model;

    // and use z-buffering
    glEnable(GL_DEPTH_TEST);
    glGenBuffers(1, &VBO1);
    copyData();
    glfwSetTime(0.0);

    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        // process input
        processInput(model, camera, window);

        // Clear the color to the appropriate mode
        if (mode==0) {
            if (glfwGetTime() < 15) {
                glClearColor(1.0f, 0.0f, 0.0f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            } else {
                glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            }
        } else if (mode==1 || mode==2){
            glClearColor(0.2f, 0.2f, 0.3f, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        } else if (mode == 5 || mode==6) {
            glClearColor(0.2f, 0.2f, 0, 1.0f);
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        }


        // activate shader
        shader.use();
        if (mode==0) {
            // Standard view mode renders all shapes
            copyDataDiscreteVBO();
            renderByDiscreteVBO(shader, camera);
        } else if (mode == 5 || mode == 6) {
            //actually the same shhh
            copyDataDiscreteVBO();
            renderActualColors(shader, camera);
        } else if (mode==1){
            // Renders the stamp and pointer
            copyStamp();
            renderStamp(shader,camera);
            pointer.use();
            copyPointer();
            renderPointer(pointer,camera);
        } else if (mode==2) {
            if (subMode == 0){
                // Renders the stamp only
                copySelectedOnly();
                renderSelectedOnly(shader, camera);
                copyStamp();
                renderStamp(shader,camera);
            }
        }

        // Swap the buffers and poll for events
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    //Terminate the glfw instance
    glfwTerminate();

    return 0;
}