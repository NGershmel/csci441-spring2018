#ifndef _CSCI441_SHAPE_H_
#define _CSCI441_SHAPE_H_

#include <cstdlib>
#include <vector>
#include "Matrix.cpp"


template <typename T, typename N>
void add_vertex(T& coords, const N& x, const N& y,
                bool with_noise=false) {
    // adding color noise makes it easier to see before shading is implemented
    float noise = 1-with_noise*(rand()%150)/100.;
    coords.push_back(x);
    coords.push_back(y);
}

//Class to hold a single shape
class Shape{
public:
    std::vector<float> coords = {0}; //Coordinates
    float myColor[3] = {0.1f,0.1f,0.1f}; //Color of the shape
    std::vector<float> boundingBox; //minX maxX minY maxY
    Matrix3 position = Matrix3(); //Position matrix
    Matrix3 rotation = Matrix3(); //Rotation matrix
    Matrix3 scale = Matrix3(); //Scale matrix
    int myGroup = -1; //Group this object belongs to
    int id = -1; //ID of the object
    int vertices = 0; //Count of vertices
    int vertexBufferSize = 0; //Size of the needed buffer
    bool selected = false; //Whether or not the object is currently selected
    bool active = false; //Whether or not the object is currently active
    bool toGroup = false; //Whether or not the object is prepped for a group
    GLuint VAO; //Vertex array object
    GLuint VBO; //Vertex buffer object

    //Refreshes the coordinate array and generates a triangle
    void generateTriangle(){
        coords = std::vector<float>();
        add_vertex(coords, -0.5f, 0.0f);
        add_vertex(coords, 0.5f, 0.0f);
        add_vertex(coords, 0.0f, 0.5f);
        vertices = 3;
        vertexBufferSize = 3*sizeof(float);
    }

    //Refreshes the coordinate array and generates a circle of n resolution
    void generateCircle(int n){
        coords = std::vector<float>();
        float step = (2*3.14159)/n;
        for (int i=0; i<n; i++) {
            add_vertex(coords, 0.5f*cos(step*i), 0.5f*sin(step*i));
            add_vertex(coords, 0.5f*cos(step*(i+1)), 0.5f*sin(step*(i+1)));
            add_vertex(coords, 0.0f, 0.0f);
        }
        vertices = n*3;
        vertexBufferSize = vertices*sizeof(float);
    }

    //Refreshes the coordinate array and generates a square
    void generateSquare(){
        coords = std::vector<float>();
        add_vertex(coords, -0.5, -0.5);
        add_vertex(coords, -0.5, 0.5);
        add_vertex(coords, 0.5, -0.5);

        add_vertex(coords, -0.5, 0.5);
        add_vertex(coords, 0.5, 0.5);
        add_vertex(coords, 0.5, -0.5);
        vertices = 6;
        vertexBufferSize = 6*sizeof(float);
    }

    //Refreshes the coordinate array and generates a mini square to be used as a pointer
    void generatePointer(){
        coords = std::vector<float>();
        add_vertex(coords, -0.01, -0.01);
        add_vertex(coords, -0.01, 0.01);
        add_vertex(coords, 0.01, -0.01);

        add_vertex(coords, -0.01, 0.01);
        add_vertex(coords, 0.01, 0.01);
        add_vertex(coords, 0.01, -0.01);
        vertices = 6;
        vertexBufferSize = 6*sizeof(float);
    }

    //Returns an array with the bounding box of the shape
    void getBoundingBox(){

    }

    //Prints the coordinates for debugging
    void printCoords(){
        std::cout << "Coordinates for object " << id << std::endl;
        for (int i=0; i<coords.size(); i+=2){
            std::cout << "X: " << coords[i] << ", Y: " << coords[i+1] << std::endl;
        }
    }

    //Returns the model matrix for the shape
    Matrix3 getModelMatrix(){
        return position * rotation * scale;
    }
};

//Class that inherits from shape to group objects
class group : Shape{
public:
    int parentGroup = -1;   //Group that this group is a part of
    std::vector<Shape> shapes;  //Shapes that are a part of this group
    std::vector<group> subgroups;   //Groups that are a part of this group
    Matrix3 position = Matrix3(); //Position matrix
    Matrix3 rotation = Matrix3(); //Rotation matrix
    Matrix3 scale = Matrix3();  //Scale matrix
    bool isGroup = true;    //Bool for polymorphism

    //Same as shape method
    Matrix3 getModelMatrix(){
        return position * rotation * scale;
    }
};

#endif
