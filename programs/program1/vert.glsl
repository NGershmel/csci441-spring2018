#version 330 core
layout (location = 0) in vec2 aPos;

uniform mat3 model;
uniform mat3 camera;

void main() {
    vec3 twoVector = vec3(aPos, 1.0);
    twoVector = twoVector * model * camera;
    gl_Position = vec4(twoVector.x, twoVector.y, 0.5, 1.0);
}
