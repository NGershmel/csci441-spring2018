# Noah Gershmel's 2D Modeler

NOTE: The program takes some time to properly boot. Due to this please wait for the screen to no
longer be red before attempting to use it and start by pressing WASD keys to refresh the camera.

The view you are first placed into is the main, view, and grouping view. The following keys are used in view mode

* Mouse. Click and drag to move the camera (panning).
* WASD. These keys move the camera in the same way as the mouse.
* - and =. These keys increase and decrease the zoom of the camera.
* Arrow keys. The left and right arrow keys loop through the objects. The GREEN object is the currently selected and active object. YELLOW objects are other objects of the same group.
* ENTER. Pressing the enter key readies the currently selected object or group to be added to a group. RED objects are objects that will be added to the group.
* G. Pressing g adds all prepped (red) objects to a group.
* U. When selecting an object with the arrow keys, pressing U will ungroup the object from whatever group it is a part of.
* M. Enters modify mode on the currently selected object or group.
* N. Enters the new shape mode.
* TAB. Enters the edit shape mode.

The new shape mode is the stamp mode. It is driven entirely by arrow keys and ENTER to move to the next step.

* Step 1: The left and right arrow keys loops through the primitives. The up and down arrow keys increase or decrease the resolution of the circle primitive.
* Step 2: The arrow keys are used to position the primitive.
* Step 3: The arrow keys are used to scale the primitive.
* Step 4: The left and right arrow keys rotate the primitive.
* Pressing enter in step 4 saves the primitive and returns you to view mode.
* Pressing tab at any time cancels the creation of the primitive.

The edit shape mode is for creating your own shapes.

* Press enter to add a vertex to the shape. Nothing will appear on the screen until the 3rd vertex.
* Continue to press enter to add triangles with 2 of the vertices being the last 2 vertices used.
* T. Press T to clear the vertex stack / start a new triangle. Another triangle will not appear until you add 3 more vertices using the enter key.
* TAB. Saves and leaves the edit mode.

The modify mode is used to modify a shape after it's creation. Primarily used for coloring.

* R and E. Increases and decreases the red value of the shape.
* G and F. Increases and decreases the green value of the shape.
* B and V. Increases and decreases the blue value of the shape.
* Arrow keys. Moves the shape(s) to a new location.
* O and P. Scales the shape(s) up or down.
* [ and ]. Rotates the shape(s).