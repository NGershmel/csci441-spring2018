#version 330 core
layout (location = 0) in vec2 aPos;
layout (location = 1) in vec3 aColor;

uniform mat3 model;
uniform mat3 camera;

out vec3 ourColor;

void main() {
    vec3 twoVector = vec3(aPos, 1.0);
    twoVector = twoVector * model * camera;
    gl_Position = vec4(twoVector.x, twoVector.y, 0.0, 1.0);
    ourColor = aColor;
}