#version 330 core
uniform vec3 selColor;
uniform vec3 objColor;

out vec4 fragColor;

void main() {
    if (selColor.x > 0.0 || selColor.y > 0.0){
        fragColor = vec4(selColor, 1.0f);
    } else {
        fragColor = vec4(objColor, 1.0f);
    }
}
