#include <iostream>

#include <glm/glm.hpp>

#include "bitmap_image.hpp"
#include "model.h"
#include "badObjLoader.h"
#include <iostream>
#define HAVE_STRUCT_TIMESPEC
#include <pthread.h>
#include <thread>

bitmap_image orth_image(640,480);
bitmap_image persp_image(640,480);
std::vector<Model> model_world;
unsigned MAX_THREADS = 0;
bool AA = true;

struct Sphere {
    int id;
    glm::vec3 center;
    glm::vec3 color;
    float radius;

    Sphere(const glm::vec3& center=glm::vec3(0,0,0),
            float radius=0,
            const glm::vec3& color=glm::vec3(0,0,0))
        : center(center), radius(radius), color(color) {
            static int id_seed = 0;
            id = ++id_seed;
        }
};

struct ray {
    glm::vec3 origin;
    glm::vec3 direction;
};

struct thread_data {
    int thread_id;
    int quad;
};

//Based on Moller-Trumbone algorithm
bool rayTriangleIntersect(const glm::vec3 &rayOrigin, const glm::vec3 &rayVector, const glm::vec3 &triangleA, const glm::vec3 &triangleB, const glm::vec3 &triangleC, glm::vec3 &intersectPoint){
    //std::cout << "testing for intersection bewtween " << rayOrigin.x << "," << rayOrigin.y << "," << rayOrigin.z << " and triangle \n" << triangleA.x << "," << triangleA.y << "," << triangleA.z << std::endl << triangleB.x << "," << triangleB.y << "," << triangleB.z << std::endl << triangleC.x << "," << triangleB.y << "," << triangleB.z << std::endl;

    const float near = 0.0000001;
    glm::vec3 edge1, edge2, h, s, q;
    float a,f,u,v;

    edge1 = triangleB - triangleA;
    edge2 = triangleC - triangleA;
    h = glm::cross(rayVector, edge2);
    a = glm::dot(edge1, h);
    if (a > -near && a<near){
        return false;
    }
    f = 1/a;
    s = rayOrigin - triangleA;
    u = f * (glm::dot(s,h));
    if (u<0.0 || u>1.0){
        return false;
    }
    q = glm::cross(s, edge1);
    v = f * glm::dot(rayVector, q);
    if (v<0.0 || u + v>1.0){
        return false;
    }
    float t = f * glm::dot(edge2, q);
    if (t>near){
        intersectPoint = rayOrigin + rayVector * t;
        return true;
    }
    else {
        return false;
    }
}

void create_image(bitmap_image &image, std::vector<std::vector<ray>> rays, int quad, float sceneHeight, float sceneWidth){
    glm::vec3 lightPos(-1, 2, 0);
    float lightDir[] = {1, -2, -0.5f};
    float ambient_strength = 0.2f;
    float specular_strength = 0.5f;
    glm::vec3 light_color(1.0, 1.0, 1.0);

    int loopCount = 1;
    if (AA) {
        loopCount = 4;
    }
    glm::vec3 AAColors[4];
    int percent_complete = 0;
    //Find intersection and color pixel per ray
    for (int y = 0; y < image.height()/MAX_THREADS; y++) {
        if (y!=0 && y%((image.height()/MAX_THREADS)/10)==0){
            percent_complete+=10;
            std::cout << "Thread " << quad << " is " << percent_complete << " percent complete" << std::endl;
        }
        for (int x = 0; x < image.width(); x++) {
            for (int l = 0; l<loopCount; l++) {
                float nearest_intersect = -INFINITY;
                glm::vec3 rayO = rays[y][x].origin;
                if (AA) {
                    float myRand = static_cast<float>(rand()) / static_cast<float>(RAND_MAX);
                    float qPixelH = (sceneHeight/image.height())*myRand;
                    float qPizelW = (sceneWidth/image.width())*myRand;
                    if (l==0){
                        rayO.x = rayO.x - qPizelW;
                        rayO.y = rayO.y - qPixelH;
                    } else if (l==1) {
                        rayO.x = rayO.x - qPizelW;
                        rayO.y = rayO.y + qPixelH;
                    } else if (l==2) {
                        rayO.x = rayO.x + qPizelW;
                        rayO.y = rayO.y - qPixelH;
                    } else if (l==3) {
                        rayO.x = rayO.x + qPizelW;
                        rayO.y = rayO.y + qPixelH;
                    }
                }

                glm::vec3 rayD = rays[y][x].direction;
                //std::cout << "Testing ray y: " << y << ", x: " << x << std::endl;
                for (Model current_model : model_world) {
                    glm::vec3 intersectPoint;
                    glm::vec3 v1(current_model.min_x, current_model.max_y, current_model.max_z);
                    glm::vec3 v2(current_model.min_x, current_model.min_y, current_model.max_z);
                    glm::vec3 v3(current_model.max_x, current_model.max_y, current_model.max_z);
                    glm::vec3 v4(current_model.max_x, current_model.min_y, current_model.max_z);

                    glm::vec3 v5(current_model.min_x, current_model.max_y, current_model.min_z);
                    glm::vec3 v6(current_model.min_x, current_model.min_y, current_model.min_z);
                    glm::vec3 v7(current_model.max_x, current_model.max_y, current_model.min_z);
                    glm::vec3 v8(current_model.max_x, current_model.min_y, current_model.min_z);
                    if (rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v1, v2, v3, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v4, v2, v3, intersectPoint) //front
                            || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v5, v6, v7, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v8, v6, v7, intersectPoint) //back
                            || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v1, v3, v5, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v7, v5, v3, intersectPoint) //top
                            || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v4, v2, v6, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v6, v4, v8, intersectPoint) //bottom
                            || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v1, v2, v5, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v2, v5, v6, intersectPoint) //left
                            || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v3, v4, v7, intersectPoint) || rayTriangleIntersect(rays[y][x].origin, rays[y][x].direction, v4, v7, v8, intersectPoint)) { //Check if orthographic ray can see
                            //std::cout << "Ray intersects bounding box" << std::endl;
                        for (int i = 0; i < current_model.coordinates.size() / 27; i++) {
                            glm::vec3 intersectPoint;
                            glm::vec3 v1(current_model.coordinates[i * 27], current_model.coordinates[i * 27 + 1],
                                             current_model.coordinates[i * 27 + 2]);
                            glm::vec3 v2(current_model.coordinates[i * 27 + 9],
                                             current_model.coordinates[i * 27 + 10],
                                             current_model.coordinates[i * 27 + 11]);
                            glm::vec3 v3(current_model.coordinates[i * 27 + 18],
                                             current_model.coordinates[i * 27 + 19],
                                             current_model.coordinates[i * 27 + 20]);
                            if (rayTriangleIntersect(rayO, rayD, v1, v2, v3, intersectPoint)) {
                                if (intersectPoint.z > nearest_intersect) {
                                        //std::cout << "Ray intersects triangle " << i << std::endl;
                                    glm::vec3 ambient = ambient_strength * light_color;
                                    glm::vec3 normal((current_model.coordinates[i * 27 + 6] +
                                                          current_model.coordinates[i * 27 + 15] +
                                                          current_model.coordinates[i * 27 + 24]) / 3,
                                                         (current_model.coordinates[i * 27 + 7] +
                                                          current_model.coordinates[i * 27 + 16] +
                                                          current_model.coordinates[i * 27 + 25]) / 3,
                                                         (current_model.coordinates[i * 27 + 8] +
                                                          current_model.coordinates[i * 27 + 17] +
                                                          current_model.coordinates[i * 27 + 26]) / 3);
                                    normal = glm::normalize(normal);

                                    glm::vec3 pixelCol = current_model.color;
                                    glm::vec3 toLightDir = glm::normalize(lightPos - intersectPoint);
                                    float diff = glm::max(glm::dot(normal, toLightDir), 0.0f);
                                    glm::vec3 diffuse = diff * light_color;

                                    glm::vec3 reflectDir = glm::reflect(-toLightDir, normal);
                                    float spec = pow(glm::max(glm::dot(rayD, reflectDir), 0.0f), 32);
                                    glm::vec3 specular = diffuse * specular_strength * spec * light_color;
                                    glm::vec3 result = (ambient + diffuse + specular) * pixelCol;
                                    AAColors[l] = result;

                                    if (!AA) {
                                        rgb_t pixel_color = make_colour(result.x * 255, result.y * 255, result.z * 255);
                                        image.set_pixel(x, y + ((image.height() / MAX_THREADS) * quad),
                                                        pixel_color);
                                    }
                                    nearest_intersect = intersectPoint.z;
                                }
                            }
                        }
                    }
                }
                if (nearest_intersect==-INFINITY){
                    AAColors[l] = glm::vec3(0.15686274509, 0.15686274509, 0.31372549019);
                }
            }
            if (AA) {
                rgb_t aa_color = make_colour(((AAColors[0].x + AAColors[1].x + AAColors[2].x + AAColors[3].x) / 4)*255,
                                             ((AAColors[0].y + AAColors[1].y + AAColors[2].y + AAColors[3].y) / 4)*255,
                                             ((AAColors[0].z + AAColors[1].z + AAColors[2].z + AAColors[3].z) / 4)*255);
                image.set_pixel(x, y + ((image.height() / MAX_THREADS) * quad), aa_color);
            }
        }
    }
}

void *render(void *threadarg) {
    thread_data *my_data;
    my_data = (thread_data*) threadarg;

    int quad = my_data->quad;

    rgb_t navy_blue = make_colour(40, 40, 80);


    //Render background
    for (int y = 0; y < orth_image.height()/MAX_THREADS; y++) {
        for (int x = 0; x < orth_image.width(); x++) {
            orth_image.set_pixel(x, y+((orth_image.height()/MAX_THREADS)*quad), navy_blue);
        }
    }

    //Create ray array
    std::vector<std::vector<ray>> rays;
    rays.resize(orth_image.height()/MAX_THREADS);
    for (int h = 0; h<orth_image.height()/MAX_THREADS; h++){
        rays[h].resize(orth_image.width());
    }

    //Create camera info
    float ratio = ((float) orth_image.width())/orth_image.height();
    //std::cout << "Ratio: " << ratio << std::endl;
    float top = 3;
    float bot = -3;
    float left = -3*ratio;
    float right = 3*ratio;

    //Create rays
    for (int y = 0; y < orth_image.height()/MAX_THREADS; y++) {
        for (int x = 0; x < orth_image.width(); x++) {
            //create ray
            ray temp_ray;
            float tempx = left + ((right-left)*(x+0.5)) / orth_image.width();
            float tempy = top + ((bot - top) * (y+((orth_image.height()/MAX_THREADS)*quad) + 0.5)) / orth_image.height();

            temp_ray.origin = glm::vec3(tempx,tempy,0);
            temp_ray.direction = glm::vec3(0,0,-1);
            rays[y][x] = temp_ray;
            //std::cout << temp_ray.origin.x << ", " << temp_ray.origin.y << ", " << temp_ray.origin.z << std::endl;
        }
    }

    create_image(orth_image, rays, quad, top-bot, right-left);

    pthread_exit(NULL);
	return 0;
}

void *render_persp(void *threadarg) {
    thread_data *my_data;
    my_data = (thread_data*) threadarg;

    int quad = my_data->quad;

    rgb_t navy_blue = make_colour(40, 40, 80);
    glm::vec3 lightPos(-1, 2, 0);
    float lightDir[] = {1, -2, -0.5f};
    float ambient_strength = 0.2f;
    float specular_strength = 0.5f;
    glm::vec3 light_color(1.0, 1.0, 1.0);

    //Render background
    for (int y = 0; y < persp_image.height()/MAX_THREADS; y++) {
        for (int x = 0; x < persp_image.width(); x++) {
            persp_image.set_pixel(x, y+((orth_image.height()/MAX_THREADS)*quad), navy_blue);
        }
    }

    //Create ray array
    std::vector<std::vector<ray>> rays;
    rays.resize(orth_image.height()/MAX_THREADS);
    for (int h = 0; h<orth_image.height()/MAX_THREADS; h++){
        rays[h].resize(orth_image.width());
    }

    //Create camera info
    float ratio = ((float) persp_image.width())/persp_image.height();
    //std::cout << "Ratio: " << ratio << std::endl;
    float top = 2;
    float bot = -2;
    float left = -2*ratio;
    float right = 2*ratio;

    //Create rays
    for (int y = 0; y < persp_image.height()/MAX_THREADS; y++) {
        for (int x = 0; x < persp_image.width(); x++) {
            //create ray
            ray temp_ray;
            float d = -2;
            float tempx = left + ((right-left)*(x+0.5)) / persp_image.width();
            float tempy = top + ((bot - top) * (y+((orth_image.height()/MAX_THREADS)*quad) + 0.5)) / persp_image.height();

            glm::vec3 pixel_point(tempx,tempy,0);
            temp_ray.origin = glm::vec3(0,0,-d);
            temp_ray.direction = glm::normalize(pixel_point - temp_ray.origin);
            rays[y][x] = temp_ray;
            //std::cout << temp_ray.direction.x << ", " << temp_ray.direction.y << ", " << temp_ray.direction.z << std::endl;
        }
    }

    create_image(persp_image, rays, quad, top-bot, right-left);

    pthread_exit(NULL);
	return 0;
}


int main(int argc, char** argv) {
    srand(static_cast<unsigned> (time(0)));
    MAX_THREADS = std::thread::hardware_concurrency();
    if (MAX_THREADS<2){
        MAX_THREADS=2;
    }
    // build world
    std::vector<Sphere> world = {
        Sphere(glm::vec3(0, 0, 1), 1, glm::vec3(1,1,0)),
        Sphere(glm::vec3(1, 1, 4), 2, glm::vec3(0,1,1)),
        Sphere(glm::vec3(2, 2, 6), 3, glm::vec3(1,0,1)),
    };

    float triangle[] = {
            1.0, 0.0, -1.0f, 0.8, 0.4, 0.2, 0, 0, 1,
            -1.0f, 0.0, -1.0f, 0.8, 0.4, 0.2, 0, 0, 1,
            0.0, 1.0, -1.0f, 0.8, 0.4, 0.2, 0, 0, 1
    };
    std::vector<float> vTriangle(triangle, triangle + sizeof(triangle)/ sizeof(float));
    Model modelTriangle = Model(vTriangle);
    //std::cout << "Bounding box x is " << modelTriangle.min_x << " ," << modelTriangle.max_x << std::endl;

    FILE* char_file = fopen("../susan.obj", "r");
    FILE* left_head = fopen("../longFace.obj", "r");
    FILE* bottom_head = fopen("../bigHead.obj", "r");
    Model head1 = createFromObjColor(char_file, 0.8f, 0.4f, 0.4f);
    Model head2 = createFromObjColor(left_head, 0.4f, 0.8f, 0.4f);
    Model head3 = createFromObjColor(bottom_head, 0.8f, 0.4f, 0.8f);
    head3.reflectiveness = 0.9;
    model_world = {
            head1,
            head2,
            head3,
    };

    // ----- Orthographic rendering -----------
    std::cout << "Orthographic ray tracing beginning..." << std::endl;
	std::vector<pthread_t> threads;
	threads.resize(MAX_THREADS);
    pthread_attr_t attr;
    void *status;
	std::vector<thread_data> td;
	td.resize(MAX_THREADS);
    int rc;

    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (int i =0; i<MAX_THREADS; i++){
        std::cout << "Starting thread " << i << std::endl;
        td[i].thread_id=i;
        td[i].quad = i;
        rc = pthread_create(&threads[i], NULL, render, (void*)&td[i]);

        if (rc) {
            std::cout << "Error creating thread " << rc << std::endl;
            exit(-1);
        }
    }

    pthread_attr_destroy(&attr);
    for (int i=0; i<MAX_THREADS; i++){
        rc = pthread_join(threads[i], &status);
        if (rc) {
            std::cout << "Error joining thread " << rc << std::endl;
            exit(-1);
        }
        std::cout << "Finished thread " << i << std::endl;
    }
    std::cout << "Finished orthographic rendering \n" << std::endl;

    orth_image.save_image("ray-traced.bmp");
    // ----- End Orthographic rendering -----------

    // ----- Perspective rendering -----------
    std::cout << "Perspective ray tracing beginning..." << std::endl;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_JOINABLE);

    for (int i =0; i<MAX_THREADS; i++){
        std::cout << "Starting thread " << i << std::endl;
        td[i].thread_id=i;
        td[i].quad = i;
        rc = pthread_create(&threads[i], NULL, render_persp, (void*)&td[i]);

        if (rc) {
            std::cout << "Error creating thread " << rc << std::endl;
            exit(-1);
        }
    }

    pthread_attr_destroy(&attr);
    for (int i=0; i<MAX_THREADS; i++){
        rc = pthread_join(threads[i], &status);
        if (rc) {
            std::cout << "Error joining thread " << rc << std::endl;
            exit(-1);
        }
        std::cout << "Finished thread " << i << std::endl;
    }
    std::cout << "Finished perspective rendering \n" << std::endl;

    persp_image.save_image("ray-traced-persp.bmp");
    // ----- End Perspective rendering -----------

    std::cout << "Success" << std::endl;

}