#ifndef _CSCI441_MODEL_H_
#define _CSCI441_MODEL_H_

class Model {

public:
    //Matrix model;
    std::vector<float> coordinates;
    glm::vec3 color;
    float reflectiveness = 0;
    int size;
    float min_x = INFINITY;
    float min_y = INFINITY;
    float min_z = INFINITY;
    float max_x = -INFINITY;
    float max_y = -INFINITY;
    float max_z = -INFINITY;
    float boundingBox[];

    template <typename Coords>
    Model(const Coords& coords) {
        size = coords.size()*sizeof(float);

        coordinates = coords;

        setBoundingBox();
    }

    Model() {

    }

    void setBoundingBox(){
        for (int i=0; i<coordinates.size()/9; i++){
            if (coordinates[i*9] < min_x) {
                min_x = coordinates[i*9];
            } if (coordinates[i*9] > max_x) {
                max_x = coordinates[i*9];
            }
            if (coordinates[i*9+1] < min_y) {
                min_y = coordinates[i*9+1];
            } if (coordinates[i*9+1] > max_y) {
                max_y = coordinates[i*9+1];
            }
            if (coordinates[i*9+2] < min_z) {
                min_z = coordinates[i*9+2];
            } if (coordinates[i*9+2] > max_z) {
                max_z = coordinates[i*9+2];
            }
        }
        boundingBox[0] = min_x;
        boundingBox[1] = max_x;
        boundingBox[2] = min_y;
        boundingBox[3] = max_y;
        boundingBox[4] = min_z;
        boundingBox[5] = max_z;
    }

    float* getBoundingBox(){
        return boundingBox;
    }
};

#endif
