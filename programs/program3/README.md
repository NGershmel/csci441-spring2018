# Ray Tracer

-There are no special instructions with this ray tracer, running the program should produce 2 images. While the program is rundering it will output to the console each threads in increments of 10%. Each object in the world is rendered using phong shading and uses a bounding box as a preliminary test on whether or not its triangles need to be tested for intersections. This greatly improves performance on scenes with many objects or where the objects take up a small portion of the screen.


-The +1's I added were CPU parallelization and Anti-Aliasing. Parallelization will happen automatically and AA is controlled by a global bool (currently on). 